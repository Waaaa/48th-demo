<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('user.login.show');
Route::post('/login', 'Auth\LoginController@login')->name('user.login');

Route::group(['middleware' => 'auth'], function(){
});
*/
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login.show');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login');
Route::get('/admin/logout', 'Auth\AdminLoginController@logout')->name('admin.login.logout');

Route::get('/admin/types', 'TypeController@index')->name('admin.types.index');
Route::get('/admin/types/create', 'TypeController@create')->name('admin.types.create');
Route::post('/admin/types', 'TypeController@store')->name('admin.types.store');
Route::get('/admin/types/{id}/edit', 'TypeController@edit')->name('admin.types.edit');
Route::patch('/admin/types/{id}', 'TypeController@update')->name('admin.types.update');
Route::delete('/admin/types/{id}', 'TypeController@delete')->name('admin.types.delete');

Route::get('/admin/stations', 'StationController@index')->name('admin.stations.index');
Route::get('/admin/stations/create', 'StationController@create')->name('admin.stations.create');
Route::post('/admin/stations', 'StationController@store')->name('admin.stations.store');
Route::get('/admin/stations/{id}/edit', 'StationController@edit')->name('admin.stations.edit');
Route::patch('/admin/stations/{id}', 'StationController@update')->name('admin.stations.update');
Route::delete('/admin/stations/{id}', 'StationController@delete')->name('admin.stations.delete');

Route::get('/admin/trains', 'TrainController@index')->name('admin.trains.index');
Route::get('/admin/trains/create', 'TrainController@create')->name('admin.trains.create');
Route::post('/admin/trains/create/setStation', 'TrainController@setStation')->name('admin.trains.setStation');
Route::get('/admin/trains/create/route/{startStationEngName}/{endStationEngName}', 'TrainController@route')->name('admin.trains.route');
//Route::match(['get', 'post'], '/admin/trains/create/route', 'TrainController@route')->name('admin.trains.route');
Route::post('/admin/trains/create', 'TrainController@store')->name('admin.trains.store');
Route::get('/admin/trains/{train_number}/edit/info', 'TrainController@editInfo')->name('admin.trains.edit.info');
Route::patch('/admin/trains/{train_number}/edit/info', 'TrainController@updateInfo')->name('admin.trains.update.info');
Route::get('/admin/trains/{train_number}/edit/station', 'TrainController@editStation')->name('admin.trains.edit.station');
Route::post('/admin/trains/{train_number}/updateStation', 'TrainController@updateStation')->name('admin.trains.update.station');
Route::get('/admin/trains/{train_number}/edit/route/{startStationEngName}/{endStationEngName}', 'TrainController@editRoute')->name('admin.trains.edit.route');
Route::patch('/admin/trains/{train_number}/edit/route', 'TrainController@updateRoute')->name('admin.trains.update.route');
Route::delete('/admin/trains/{train_number}', 'TrainController@delete')->name('admin.trains.delete');

//Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/trains', 'TrainController@userIndex')->name('user.trains.index');
Route::get('/trains/{train_number}', 'TrainController@show')->where('train_number', '[0-9]+')->name('user.trains.show');
Route::get('/trains/search', 'TrainController@searchByNumber')->name('user.trains.search.by.number');
Route::post('/trains', 'TrainController@NumberSearchResult')->name('user.trains.number.search.result');
Route::get('/', 'TrainController@searchTrain')->name('user.trains.search');
Route::get('/trains/result/{boardingDate}/{startStationEngName}/{endStationEngName}/{type_id}', 'TrainController@TrainSearchResult')->name('user.trains.search.result');
//Route::match(['get', 'post'], '/tickets/create', 'TicketController')->name('tickets.create');

Route::get('/tickets', 'TicketController@index')->name('user.tickets.index');
//Route::get('/tickets/create', 'TicketController@create')->name('user.tickets.create');
Route::match(['get', 'post'], '/tickets/create', 'TicketController@create')->name('user.tickets.create');
Route::post('/tickets', 'TicketController@store')->name('user.tickets.store');
Route::get('/tickets/search', 'TicketController@search')->name('user.tickets.search');
Route::get('/tickets/{ticketNumber}', 'TicketController@show')->name('user.tickets.show');
Route::post('/tickets/search/result', 'TicketController@searchResult')->name('user.tickets.search.result');
Route::delete('/tickets/cancel/{ticketNumber}', 'TicketController@cancel')->name('user.tickets.cancel');

Route::get('/admin/tickets/search', 'TicketController@adminSearch')->name('admin.tickets.search');
Route::post('/admin/tickets/search/result', 'TicketController@adminSearchResult')->name('admin.tickets.search.result');
Route::delete('/admin/tickets/cancel/{ticketNumber}', 'TicketController@adminCancel')->name('admin.tickets.cancel');




