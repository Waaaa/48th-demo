# 火車訂票系統
[**火車訂票系統**](https://arcane-refuge-53978.herokuapp.com/)  
第48屆全國技能競賽暨第48屆國際技能競賽國手選拔賽模擬試題

## 系統功能說明：
### (一)、前台模組
**車次查詢功能**  
首頁提供車次查詢的功能，送出方式為GET，可依：
* 起程站*
* 到達站*
* 車種*
* 搭乘日期*

這四種條件查詢車次，進入查詢結果後，可看到：
* 車種
* 列車編號
* 發車站
* 終點站
* 預計開車時間
* 預計到達時間
* 行駛時間
* 票價

及訂票按鈕等資訊，訂票按鈕點選後會自動將起訖站、日期及車次等資訊帶入訂票功能相關欄位，若查無指定條件車次，系統將顯示訊息提示使用者並回到車次查詢頁面。

**訂票功能**  
使用者須輸入：
* 手機號碼*
* 起程站*
* 到達站搭乘日期*
* 車次代碼*
* 車票張數*

等資料，若有以下情況，將會提示錯誤訊息並回到訂票頁面：
* 任一欄位為空
* 發車時間已過
* 當日無該車次列車
* 該列車無行經起訖站
* 該區間已無空位

訂票成功後，系統將會顯示以下資訊：
* 訂票編號
* 手機號碼
* 發車時間
* 車次、起訖站
* 張數、車票單價
* 總票價

**查詢訂票功能**  
使用者可以透過輸入訂票編號或是手機號碼查詢訂票紀錄，查詢成功時系統將會顯示顯示以下資料：
* 訂票編號
* 訂票時間
* 發車時間
* 車次、起訖站
* 張數

若查無資料，系統將提示錯誤訊息並回到查詢頁面，並提供頁碼切換功能，每頁最多10筆資料

**查詢列車資訊功能**  
輸入車次代碼後，可以查詢該車次的以下資訊：
* 行駛星期
* 各車站的抵達時間及發車時間

若查無指定條件車次，系統將顯示訊息提示使用者並回到列車資訊功頁面。

### (二)、後臺管理模組
**車種管理**  
管理員可以新增/編輯/刪除車種，系統內建有以下車種：區間列車、快速列車、磁浮列車，列車必須保含有：車種名稱及最高時速(km/h)等資訊，刪除車種時，屬於該車種的列車必須同時被刪除。

**車站管理**  
管理員可以新增/編輯/刪除車站，系統內建有以下車站：
* 台北站
* 桃園站
* 新竹站
* 苗栗站
* 台中站
* 彰化站
* 雲林站
* 嘉義站
* 台南站
* 高雄站
* 屏東站
* 台東站
* 花蓮站
* 宜蘭站

車站必須包含有：車站名稱及英文名稱等資訊。刪除車站時，若有列車會停靠該站，系統將會提示錯誤訊息，禁止刪除並回到車站管理頁面。

**列車管理**  
管理員可以新增/編輯/刪除列車，列車必須包含以下資訊：
* 列車代碼
* 行車星期
* 發車時間
* 行經車站管理(必須設定一個發車站和終點站、可以設定各車站的停留時間與前一車站到該站的行駛時間(發車站除外))
* 單一車廂的載客數量
* 車輛數量

系統將會內建有一些列車，當列車被刪除時，將刪除該列車相關的訂票紀錄。

**訂票紀錄查詢**  
管理員可以依以下條建查詢符合條件的訂票紀錄：
* 發車日期*
* 車次*
* 手機號碼
* 訂票編號
* 起訖站*

管理員可以取消尚未發車的訂單，系統提供頁碼切換的功能，每頁最多10筆資料。

## 開發工具與設備環境
* bootstrap 4.0.0
* jquery 3.3.1
* Laravel 5.4.36
* PHP 7.3.5
* Apache 2.4
* MariaDB 10.1.40

## 資料庫規劃
```sql
--
-- 資料庫： `train`
--

-- --------------------------------------------------------

--
-- 資料表結構 `driving_weeks`
--

CREATE TABLE `driving_weeks` (
  `id` int(10) UNSIGNED NOT NULL,
  `train_id` int(11) NOT NULL,
  `week` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `routes`
--

CREATE TABLE `routes` (
  `id` int(10) UNSIGNED NOT NULL,
  `station_id` int(11) NOT NULL,
  `stay_time` int(11) NOT NULL,
  `driving_time` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `train_id` int(11) NOT NULL,
  `last` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `stations`
--

CREATE TABLE `stations` (
  `id` int(10) UNSIGNED NOT NULL,
  `chinese_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `tickets`
--

CREATE TABLE `tickets` (
  `id` int(10) UNSIGNED NOT NULL,
  `cell_phone_number` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_station_id` int(11) NOT NULL,
  `end_station_id` int(11) NOT NULL,
  `boarding_date` date NOT NULL,
  `train_number` int(11) NOT NULL,
  `number_of_tickets` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_cancel` tinyint(1) NOT NULL DEFAULT '0',
  `canceled_at` datetime DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `trains`
--

CREATE TABLE `trains` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` int(11) NOT NULL,
  `departure_time` time NOT NULL,
  `passenger_number` int(11) NOT NULL,
  `cabin_number` int(11) NOT NULL,
  `passenger_total_number` int(11) NOT NULL,
  `start_station_id` int(11) NOT NULL,
  `end_station_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `highest_speed` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `driving_weeks`
--
ALTER TABLE `driving_weeks`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `stations`
--
ALTER TABLE `stations`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `trains`
--
ALTER TABLE `trains`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動增長(AUTO_INCREMENT)
--

--
-- 使用資料表自動增長(AUTO_INCREMENT) `driving_weeks`
--
ALTER TABLE `driving_weeks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `stations`
--
ALTER TABLE `stations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `trains`
--
ALTER TABLE `trains`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

```

### 系統預設資料
```php
$ php artisan migrate --seed
```

```php
//預設管理者資料
[
  'account' => 'admin',
  'name' => '火車管理員',
  'email' => 'admin@email.com',
  'password' => Hash::make('cdertgbvf'),
  'created_at' => Carbon::now()->format('Y-m-d H:i:s')
]

//預設使用者資料
[
    'account' => 'user',
    'name' => '一般使用者',
    'email' => 'user@email.com',
    'password' => Hash::make('cdertgbvf'),
    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
]

//預設車站資料
[
  '台北' => 'Taipei',
  '桃園' => 'Taoyuan',
  '新竹' => 'Hsinchu',
  '苗栗' =>'Miaoli',
  '台中' => 'Taichung',
  '彰化' => 'Changhua',
  '雲林' => 'Yunlin',
  '嘉義' => 'Chiayi',
  '台南' => 'Tainan',
  '高雄' => 'Kaohsiung',
  '屏東' => 'Pingtung',
  '台東' => 'Taitung',
  '花蓮' => 'Hualien',
  '宜蘭' => 'Yilan',
]

//預設車種資料
[
  '區間列車' => '100',
  '快速列車' => '115',
  '磁浮列車' => '120',
]

//預設車次資料
[
    ['number' => '803',
     'departure_time' => '06:26:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '1607',
     'departure_time' => '07:21:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 5, 8, 9, 10],
     'driving_weeks' => [6],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '1305',
     'departure_time' => '08:01:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 5, 6, 7, 8, 9, 10],
     'driving_weeks' => [5],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '117',
     'departure_time' => '09:31:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 5, 10],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '817',
     'departure_time' => '10:11:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '821',
     'departure_time' => '11:11:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '1637',
     'departure_time' => '12:21:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 5, 8, 9, 10],
     'driving_weeks' => [5],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '133',
     'departure_time' => '13:31:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 5, 10],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '651',
     'departure_time' => '12:21:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 5, 8, 9, 10],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '1237',
     'departure_time' => '15:51:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 5, 10],
     'driving_weeks' => [5, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '1541',
     'departure_time' => '16:36:00', 
     'start_station_id' => 1, 
     'end_station_id' => 5, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 4, 5],
     'driving_weeks' => [5, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '149',
     'departure_time' => '17:31:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 5, 10],
     'driving_weeks' => [5, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '673',
     'departure_time' => '18:21:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 5, 8, 9, 10],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '853',
     'departure_time' => '19:11:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '681',
     'departure_time' => '19:46:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 5, 8, 9, 10],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '1557',
     'departure_time' => '20:36:00', 
     'start_station_id' => 1, 
     'end_station_id' => 5, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 4, 5],
     'driving_weeks' => [5, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '165',
     'departure_time' => '21:31:00', 
     'start_station_id' => 1, 
     'end_station_id' => 10, 
     'type_id' => '1',
     'routes' => [1, 5, 10],
     'driving_weeks' => [5, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '565',
     'departure_time' => '22:26:00', 
     'start_station_id' => 1, 
     'end_station_id' => 5, 
     'type_id' => '1',
     'routes' => [1, 2, 3, 4, 5],
     'driving_weeks' => [5, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '502',
     'departure_time' => '06:05:00', 
     'start_station_id' => 5, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [5, 4, 3, 2, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '802',
     'departure_time' => '06:15:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '606',
     'departure_time' => '07:00:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '108',
     'departure_time' => '07:55:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 5, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '112',
     'departure_time' => '08:55:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 5, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '1622',
     'departure_time' => '09:35:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [1, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '628',
     'departure_time' => '10:35:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '124',
     'departure_time' => '11:55:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 5, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '642',
     'departure_time' => '13:00:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '648',
     'departure_time' => '14:00:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '834',
     'departure_time' => '14:25:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '1318',
     'departure_time' => '14:45:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '838',
     'departure_time' => '15:25:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '1550',
     'departure_time' => '17:04:00', 
     'start_station_id' => 5, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [5, 4, 3, 2, 1],
     'driving_weeks' => [5, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '666',
     'departure_time' => '17:00:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '148',
     'departure_time' => '17:55:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 5, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '676',
     'departure_time' => '17:00:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '854',
     'departure_time' => '19:25:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 100
     ],
     ['number' => '684',
     'departure_time' => '20:00:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
     ['number' => '160',
     'departure_time' => '20:55:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 5, 1],
     'driving_weeks' => [1, 2, 3, 4, 5, 6, 7],
     'stay_time' => 5,
     'driving_time' => 60,
     'plusPrice' => 600
     ],
     ['number' => '696',
     'departure_time' => '21:55:00', 
     'start_station_id' => 10, 
     'end_station_id' => 1, 
     'type_id' => '1',
     'routes' => [10, 9, 8, 5, 3, 2, 1],
     'driving_weeks' => [7],
     'stay_time' => 5,
     'driving_time' => 30,
     'plusPrice' => 150
     ],
]
```