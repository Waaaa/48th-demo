<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_types = [
    		'區間列車' => '100',
    		'快速列車' => '115',
    		'磁浮列車' => '120',
    	];

    	foreach ($arr_types as $typeName => $highestSpeed) {
    		DB::table('types')->insert([
	        	'type_name' => $typeName,
	        	'highest_speed' => $highestSpeed,
	        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
	        ]);
    	}
    }
}
