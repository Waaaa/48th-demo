<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('admins')->insert([
        	'account' => 'admin',
        	'name' => '火車管理員',
        	'email' => 'admin@email.com',
        	'password' => Hash::make('cdertgbvf'),
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'account' => 'user',
            'name' => '一般使用者',
            'email' => 'user@email.com',
            'password' => Hash::make('cdertgbvf'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
