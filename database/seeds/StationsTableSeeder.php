<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$arr_stations = [
    		'台北' => 'Taipei',
    		'桃園' => 'Taoyuan',
    		'新竹' => 'Hsinchu',
    		'苗栗' =>'Miaoli',
    		'台中' => 'Taichung',
    		'彰化' => 'Changhua',
    		'雲林' => 'Yunlin',
    		'嘉義' => 'Chiayi',
    		'台南' => 'Tainan',
    		'高雄' => 'Kaohsiung',
    		'屏東' => 'Pingtung',
    		'台東' => 'Taitung',
    		'花蓮' => 'Hualien',
    		'宜蘭' => 'Yilan',
    	];

    	$sequence = 1;
    	foreach ($arr_stations as $chineseName => $englishName) {
    		DB::table('stations')->insert([
	        	'chinese_name' => $chineseName,
	        	'english_name' => $englishName,
	        	'sequence' => $sequence,
	        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
	        ]);
    		$sequence+=1;
    	}
    }
}
