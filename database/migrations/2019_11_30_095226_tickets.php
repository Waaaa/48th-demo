<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //訂票資訊
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cell_phone_number'); //連絡電話
            $table->integer('start_station_id')->foreign('start_station_id')->references('id')->on('stations'); //啟程站id
            $table->integer('end_station_id')->foreign('end_station_id')->references('id')->on('stations'); //啟程站id
            $table->date('boarding_date'); //搭乘日期
            $table->integer('train_number')->foreign('train_number')->references('number')->on('trains'); //列車代號
            $table->integer('number_of_tickets'); //訂購張數
            $table->timestamps();
            $table->boolean('is_cancel')->default(false);
            $table->dateTime('canceled_at')->nullable();
            $table->boolean('is_delete')->default(false);
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
