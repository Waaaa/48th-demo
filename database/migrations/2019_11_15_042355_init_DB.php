<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        //車站資訊
        Schema::create('stations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chinese_name')->uniqle(); //中文車站名稱
            $table->string('english_name')->uniqle(); //英文車站名稱
            $table->integer('sequence'); //車站排序(北下順序)
            $table->timestamps();
        });

        //車種資訊
        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_name'); //車種名稱
            $table->integer('highest_speed'); //最高時速(km/h)
            $table->timestamps();
        });

        //列車資訊
        Schema::create('trains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->uniqle(); //列車代碼
            $table->time('departure_time'); //發車時間
            $table->integer('passenger_number'); //單一車廂的載客數量
            $table->integer('cabin_number'); //車廂數量
            $table->integer('passenger_total_number'); //總載客數量
            $table->integer('start_station_id')->foreign('start_station_id')->references('id')->on('stations'); //發車站id
            $table->integer('end_station_id')->foreign('end_station_id')->references('id')->on('stations'); //終點站站id
            $table->integer('type_id')->foreign('type_id')->references('id')->on('types'); //車種id
            $table->timestamps();
        });

        //列車行駛星期資訊
        Schema::create('driving_weeks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('train_id')->foreign('train_id')->references('id')->on('trains'); //列車id
            $table->integer('week'); //星期
        });

        //列車停靠資訊
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('station_id')->foreign('station_id')->references('id')->on('stations'); //車站id
            $table->integer('stay_time'); //停留時間
            $table->integer('driving_time'); //前一站到該站的行駛時間
            $table->integer('price'); //票價
            $table->integer('train_id')->foreign('train_id')->references('id')->on('trains'); //列車id
            $table->boolean('last')->default(false); //是否為最終站
        });

        //使用者資訊
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        //使用者回復密碼(Auth預設)
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
        Schema::dropIfExists('types');
        Schema::dropIfExists('trains');
        Schema::dropIfExists('driving_weeks');
        Schema::dropIfExists('routes');
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
    }
}
