<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    public $timestamps = false;

    //建立關聯
    //停靠的車站
    function station(){
    	return $this->hasOne('App\Station', 'id', 'station_id');
    }

    //行駛的列車
    function train(){
    	return $this->hasOne('App\Train', 'id', 'train_id');
    }

    //accessor
    //計算抵達時間
    public function getArrivalTimeAttribute(){
        $routes = $this->train->routes;
        $totalDrivingTime = 0;
        $i = 0;
        foreach ($routes as $route) {
            if($route->id == $this->id+1){
                break;
            }
            $totalDrivingTime += $route->driving_time;

            //第一站及本站的停靠時間不算
            if( $i != 0 && $route->id < $this->id){
                $totalDrivingTime += $route->stay_time;
            }
            $i++;
        }

        //出發時間加上到該站的總行駛時間
        $arrivalTime = date("H:i",strtotime($this->train->departure_time."+".$totalDrivingTime." min"));
        return $arrivalTime;
    }
}
