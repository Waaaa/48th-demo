<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrivingWeek extends Model
{
    public $timestamps = false;
    protected $table = 'driving_weeks';
    //建立關聯
    //行駛的列車
    function train(){
    	return $this->hasOne('App\Train', 'id', 'train_id');
    }

    //accessor
    public function getChineseNameAttribute(){
    	$arr_chineseNames = ['一', '二', '三', '四', '五', '六', '日'];
    	$index = $this->week - 1;
    	return $arr_chineseNames[$index];
    }
}
