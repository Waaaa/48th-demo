<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{

    //定義關聯
    //從此站出發的火車
    function startTrains(){
    	$stations = $this->hasMany('App\Train', 'start_station_id', 'id');
    	return $stations;
    }
    //終點站為此站的火車
    function endTrains(){
    	$stations = $this->hasMany('App\Train', 'end_station_id', 'id');
    	return $stations;
    }
    //有停靠此站的路線
    function routes(){
    	$routes = $this->hasMany('App\Route', 'station_id', 'id')->where('stay_time','>',0);
    	return $routes;
    }
}
