<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class adminCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Session::has('adminLogin') ||  ! Session::get('adminLogin') == "true") {
            abort(404,'頁面不存在');
            return;         
        }
        if ( ! $request->is('admin/trains/create/*') && ! $request->is('admin/trains/create') ) {
            if(Session::has('info')){
                Session::forget('info');
            }
        }
        if ( ! $request->is('admin/trains/*/edit/info') && ! $request->is('admin/trains/*/edit/route') && ! $request->is('admin/trains/*/edit') ) {
            if(Session::has('editInfo')){
                Session::forget('editInfo');
            }
        }


        return $next($request);
    }
}
