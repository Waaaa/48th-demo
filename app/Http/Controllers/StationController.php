<?php

namespace App\Http\Controllers;

use App\Station;
use DB;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class StationController extends Controller
{
    public function __construct()
	{
	    $this->middleware('auth:admin');
	}
	public function index($order = 'north'){
		if($order == 'south'){
			$stations = Station::orderBy('sequence', 'desc')->get();
		}else{
			$stations = Station::orderBy('sequence', 'asc')->get();
		}
		
		return view('admin.stations.index')->with(['stations' => $stations, 'order' => $order]);
	}

	public function create(){
		$stations = Station::orderBy('sequence', 'asc')->get();
		return view('admin.stations.create')->with(['stations' => $stations]);
	}

	public function store(Request $request){
    	$rules = ['chineseName'=>'required|unique:stations,chinese_name',
	              'englishName'=>'required|unique:stations,english_name',
	              'sequence' => 'required|numeric|between:0,'.Station::count()];
		$this->validate($request, $rules);

		try {
			DB::beginTransaction();

			//排序車站
			if($request->sequence != Station::count()){
				$edit_stations = Station::where('sequence','>',$request->sequence)->get();
				foreach ($edit_stations as $edit_station) {
					$sequence = $edit_station->sequence;
					$edit_station->sequence = $sequence + 1;
					$edit_station->save();
				}
			}
			
			$station = new Station;
			$station->chinese_name = $request->chineseName;
			$station->english_name = $request->englishName;
			$station->sequence = $request->sequence + 1;
			$station->save();
			DB::commit();
			Session::flash('success', '新增車站成功');
			return redirect()->route('admin.stations.index');
		} catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '新增車站失敗，請聯絡網站管理員');
			return redirect()->route('admin.stations.index');
		}
	}

	public function edit($id){
		$station = Station::findOrfail($id);
		$stations = Station::where('id', '<>', $id)->orderBy('sequence', 'asc')->get();
		return view('admin.stations.edit')->with(['station' => $station, 'stations' => $stations]);
	}

	public function update(Request $request, $id){
		$rules = ['chineseName'=>'required|unique:stations,chinese_name,'.$id,
	              'englishName'=>'required|unique:stations,english_name,'.$id,
	              'sequence' => 'required|numeric|between:0,'.Station::count()];
		$this->validate($request, $rules);

		//判斷是否有列車經過該車站
		$station = Station::findOrfail($id);
		$number = $station->startTrains->count() + $station->endTrains->count() + $station->routes->count();
		if($number > 0 && ($request->sequence+1) != $station->sequence ){
			return redirect()->route('admin.stations.edit',[ 'id' => $station->id ])
							 ->withErrors(['sequence' => '尚有列車停靠'.$station->chineseName.'，不能修改順序車站'])
							 ->withInput();
		}

		try {
			DB::beginTransaction();
		
			//修改車站排序
			if(($request->sequence+1) != $station->sequence){
				if($station->sequence < $request->sequence){
					// 由小變大(ex:2 -> 5, $request->sequence = 5)
					$edit_stations = Station::where('sequence','>',$station->sequence)->where('sequence','<=',$request->sequence)->get();
					foreach ($edit_stations as $edit_station) {
						$sequence = $edit_station->sequence;
						$edit_station->sequence = $sequence - 1;
						$edit_station->save();
					}
		    		$station->sequence = $request->sequence;
				}else{
					// 由大變小(ex:5 -> 2, $request->sequence = 1)
					$edit_stations = Station::where('sequence','>',$request->sequence)->where('sequence','<',$station->sequence)->get();
					foreach ($edit_stations as $edit_station) {
						$sequence = $edit_station->sequence;
						$edit_station->sequence = $sequence + 1;
						$edit_station->save();
					}
		    		$station->sequence = $request->sequence + 1;
				}
			}
			$station->chinese_name = $request->chineseName;
			$station->english_name = $request->englishName;
			$station->save();
			DB::commit();
			Session::flash('success', '修改車站成功');
			return redirect()->route('admin.stations.index');
		} catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '修改車站失敗，請聯絡網站管理員');
			return redirect()->route('admin.stations.index');
		}
	}

	public function delete($id){
		$station = Station::findOrfail($id);

		//判斷是否有列車經過該車站
		$number = $station->startTrains->count() + $station->endTrains->count() + $station->routes->count();
		if($number > 0){
			Session::flash('error', '尚有列車停靠'.$station->chinese_name.'，不能刪除車站');
			return redirect()->route('admin.stations.index');
		}

		try {
			DB::beginTransaction();

			//修改車站排序
			$edit_stations = Station::where('sequence','>',$station->sequence)->get();
			foreach ($edit_stations as $edit_station) {
				$sequence = $edit_station->sequence;
				$edit_station->sequence = $sequence - 1;
				$edit_station->save();
			}

			$station->delete();
			DB::commit();
			Session::flash('success', '車站刪除成功');
	    	return redirect()->route('admin.stations.index');
		} catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '刪除車站失敗，請聯絡網站管理員');
			return redirect()->route('admin.stations.index');
		}
	}

}
