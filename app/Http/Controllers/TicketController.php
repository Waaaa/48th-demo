<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Station;
use App\Train;
use App\DrivingWeek;
use App\Ticket;
use App\Route;
use DB;
use Session;
use Auth;

class TicketController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth:admin', ['only' => ['adminSearch', 'adminSearchResult', 'adminCancel']]);
	}

    public function index(){
    	return redirect()->route('user.tickets.create');
    }

    public function create(Request $request){
    	$stations = Station::get();
        $input = array();

        //從訂票按鈕過來
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->only(['trainNumber', 'startStation', 'endStation']), [
                'trainNumber'=>'required|numeric|exists:trains,number',
                'startStation'=>'required|exists:stations,english_name|not_in:'.$request->endStation,
                'endStation'=>'required|exists:stations,english_name|not_in:'.$request->startStation
            ]);

            if(!$validator->fails()){
                $train = Train::where('number', $request->trainNumber)->first();
                $startStation = Station::where('english_name', $request->startStation)->first();
                $endStation = Station::where('english_name', $request->endStation)->first();

                //判斷列車是否有停靠起訖站
                $arr_routeInfomation = $train->getRouteInfomationAttribute();
                if( (array_key_exists($startStation->id, $arr_routeInfomation) && $arr_routeInfomation[$startStation->id]['departureTime'] > 0) && (array_key_exists($endStation->id, $arr_routeInfomation) && $arr_routeInfomation[$endStation->id]['departureTime'] > 0) ){
                    $input['trainNumber'] = $train->number;
                    $input['startStation'] = $request->startStation;
                    $input['endStation'] = $request->endStation;
                }
            }
        }

    	return view('user.tickets.create')->with(["stations" => $stations, "input" => $input]);
    }

    public function store(Request $request){
    	$rules = [
    		"cellPhoneNumber" => "required|regex:/(^09[0-9]{2}-[0-9]{6}$)/",
    		'startStation'=>'required|exists:stations,english_name|not_in:'.$request->endStation,
	        'endStation'=>'required|exists:stations,english_name|not_in:'.$request->startStation,
	        'boardingDate'=>'required|date_format:Y-m-d',
	        'trainNumber'=>'required|numeric|exists:trains,number',
	        'numberOfTickets'=>'required|numeric|between:1,100',
    	];
    	$this->validate($request, $rules);

    	//判斷搭乘日期
    	if(strtotime(date('Y-m-d', time())) > strtotime($request->boardingDate)){
    		return redirect()->route('user.tickets.create')
	    			->withErrors(['boardingDate'=>'請輸入有效的日期'])
	    			->withInput();
    	}

    	$train = Train::where('number', $request->trainNumber)->firstOrfail();
    	$startStation = Station::where('english_name', $request->startStation)->firstOrfail();
    	$endStation = Station::where('english_name', $request->endStation)->firstOrfail();

    	//判斷當日有無該車次的列車
    	//將日期轉換為星期
	    $arr_weekEngNames = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
	    $week = array_search(date("D",strtotime($request->boardingDate)), $arr_weekEngNames)+1;
	    if(DrivingWeek::where('train_id', $train->id)->where('week', $week)->count() <= 0){
	    	return redirect()->route('user.tickets.create')
	    			->withErrors(['boardingDate'=>'當日無該車次的列車'])
	    			->withInput();
	    }

    	//判斷該列車有無行經起訖站
    	if($train->start_station_id != $startStation->id){
    		if(Route::where('train_id', $train->id)->where('station_id', $startStation->id)->where('stay_time', '>', 0)->count() <= 0){
    			return redirect()->route('user.tickets.create')
	    			->withErrors(['startStation'=>'該列車無行經起訖站'])
	    			->withInput();
    		}
    	}

    	if($train->end_station_id != $endStation->id){
    		if(Route::where('train_id', $train->id)->where('station_id', $endStation->id)->where('stay_time', '>', 0)->count() <= 0){
    			return redirect()->route('user.tickets.create')
	    			->withErrors(['endStation'=>'該列車無行經起訖站'])
	    			->withInput();
    		}
    	}

    	//判斷發車時間是否已過
    	if(strtotime($request->boardingDate.' '.$train->route_infomation[$startStation->id]['departureTime']) < strtotime(date("Y-m-d H:i", time()))){
    		return redirect()->route('user.tickets.create')
	    			->withErrors(['trainNumber'=>'發車時間已過'])
	    			->withInput();
    	}

    	//判斷該區間是否還有座位
    	if($startStation->sequence < $endStation->sequence){
    		//南下列車
    		$currentTotalPassengers = DB::table('tickets')->where('train_number', $train->number)
    											->join('stations as start_station', 'tickets.start_station_id', '=', 'start_station.id')
    											->join('stations as end_station', 'tickets.end_station_id', '=', 'end_station.id')
    											->where('start_station.sequence', '>=', $startStation->sequence)
    											->where('end_station.sequence', '<=', $endStation->sequence)
    											->count();
    	}else{
    		//北上列車
    		$currentTotalPassengers = DB::table('tickets')->where('train_number', $train->number)
    											->join('stations as start_station', 'tickets.start_station_id', '=', 'start_station.id')
    											->join('stations as end_station', 'tickets.end_station_id', '=', 'end_station.id')
    											->where('start_station.sequence', '<=', $startStation->sequence)
    											->where('end_station.sequence', '>=', $endStation->sequence)
    											->count();
    	}
    	if(($currentTotalPassengers >= $train->passenger_total_number) || $request->numberOfTickets > ($train->passenger_total_number - $currentTotalPassengers)){
    		return redirect()->route('user.tickets.create')
	    			->withErrors(['numberOfTickets'=>'該區間已無足夠的空車位'])
	    			->withInput();
    	}

    	try{
	    	DB::beginTransaction();

	    	$ticket = new Ticket();
	    	$ticket->number = substr(md5(uniqid(rand(), true)),0,10);
	    	$ticket->cell_phone_number = $request->cellPhoneNumber;
	    	$ticket->start_station_id = $startStation->id;
	    	$ticket->end_station_id = $endStation->id;
	    	$ticket->boarding_date = $request->boardingDate;
	    	$ticket->train_number = $train->number;
	    	$ticket->number_of_tickets = $request->numberOfTickets;
	    	$ticket->save();

			DB::commit();
			Session::flash('success', '預訂車票成功');
			return redirect()->route('user.tickets.show', ['ticketNumber' => $ticket->number]);
	    } catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '預訂車票失敗，請聯絡網站管理員');
			return redirect()->route('user.tickets.index');
		}
    }

    public function show($ticketNumber){
    	$ticket = Ticket::where('number', $ticketNumber)->firstOrfail();

    	return view('user.tickets.show', ['ticket' => $ticket]);
    }

    public function search(){
    	return view('user.tickets.search');
    }

    public function searchResult(Request $request){
    	$rules = [
    		"ticketNumber"=>"regex:/(^[A-z0-9]{10}$)/|nullable",
    		"cellPhoneNumber"=>"regex:/(^09[0-9]{2}-[0-9]{6}$)/|nullable",
    	];
    	$this->validate($request, $rules);
    	/*
    	$ticketsCount = Ticket::where('number', $request->ticketNumber)->orWhere('cell_phone_number', $request->cellPhoneNumber)->count();
    	if($ticketsCount <= 0){
    		return redirect()->route('user.tickets.search')
    						->withErrors(['ticket'=>'查無訂票紀錄'])
    						->withInput();
    	}*/

    	$tickets = Ticket::where('number', $request->ticketNumber)->orWhere('cell_phone_number', $request->cellPhoneNumber)->orderBy('created_at', 'asc')->paginate(10);
    	
    	//計算頁數
		if($tickets->lastPage() > 0){
    		//計算頁數
			if($tickets->lastPage() < $tickets->currentPage()){
				abort(404,'頁面不存在');
				exit;
			}
    	}
    	

    	return view('user.tickets.result', ['tickets' => $tickets, 'ticketNumber' => $request->ticketNumber, 'cellPhoneNumber' => $request->cellPhoneNumber]);
    }

    public function cancel($ticketNumber){
    	$ticket = Ticket::where('number', $ticketNumber)->firstOrfail();

    	if(strtotime($ticket->boarding_date." ".$ticket->train->route_infomation[$ticket->start_station_id]['departureTime']." -30min") <= time()){
    		Session::flash('error', '取消訂票失敗，列車出發前30分鐘內無法取消訂票');
    		return redirect()->route('user.tickets.show', ['ticketNumber' => $ticket->number]);
    	}

    	try{
    		DB::beginTransaction();

    		$ticket->is_cancel = true;
    		$ticket->canceled_at = date('Y-m-d H:i:s', time());
    		$ticket->save();

    		DB::commit();
			Session::flash('success', '取消訂票成功');
    	}catch(Exception $e){
    		DB::rollback();
			Session::flash('error', '取消訂票失敗，請聯絡網站管理員');
    	}
    	return redirect()->route('user.tickets.show', ['ticketNumber' => $ticket->number]);
    }

    public function adminSearch(){
    	$stations = Station::get();
    	return view("admin.tickets.search", ["stations" => $stations]);
    }

    public function adminSearchResult(Request $request){
    	$rules = [
    		'boardingDate'=>'date_format:Y-m-d|nullable',
    		'trainNumber'=>'numeric|exists:trains,number|nullable',
    		"cellPhoneNumber" => "regex:/(^09[0-9]{2}-[0-9]{6}$)/|nullable",
    		'startStation'=>'exists:stations,english_name|not_in:'.$request->endStation."|nullable",
	        'endStation'=>'exists:stations,english_name|not_in:'.$request->startStation."|nullable",
    	];
    	$this->validate($request, $rules);

    	/*
    	if($request->has('startStation')){
    		$startStation = Station::where("english_name", $request->startStation)->first();
    	}*/
    	$startStation = Station::where("english_name", $request->startStation)->first();
    	$endStation = Station::where("english_name", $request->endStation)->first();

    	($startStation) ? $startStationId = $startStation->id : $startStationId = "";
    	($endStation) ? $endStationId = $endStation->id : $endStationId = "";

    	$tickets = Ticket::where("boarding_date", $request->boardingDate)
    						->orWhere("train_number", $request->trainNumber)
    						->orWhere("cell_phone_number", $request->cellPhoneNumber)
    						->orWhere("start_station_id", $startStationId)
    						->orWhere("end_station_id", $endStationId)
    						->paginate(10);

    	if($tickets->lastPage() > 0){
    		//計算頁數
			if($tickets->lastPage() < $tickets->currentPage()){
				abort(404,'頁面不存在');
				exit;
			}
    	}

    	return view("admin.tickets.result", ["tickets" => $tickets]);
    }

    public function adminCancel($ticketNumber){
    	$ticket = Ticket::where('number', $ticketNumber)->firstOrfail();

    	if(strtotime($ticket->boarding_date." ".$ticket->train->route_infomation[$ticket->start_station_id]['departureTime']) <= time()){
    		Session::flash('error', Auth::user()->name.'取消訂票('.$ticket->number.')失敗，列車出發後無法取消訂票');
    		return redirect()->route('admin.tickets.search');
    	}

    	try{
    		DB::beginTransaction();

    		$ticket->is_delete = true;
    		$ticket->deleted_at = date('Y-m-d H:i:s', time());
    		$ticket->save();

    		DB::commit();
			Session::flash('success', Auth::user()->name.'取消訂票('.$ticket->number.')成功');
    	}catch(Exception $e){
    		DB::rollback();
			Session::flash('error', Auth::user()->name.'取消訂票('.$ticket->number.')失敗，請聯絡網站管理員');
    	}
    	return redirect()->route('admin.tickets.search');
    }
}
