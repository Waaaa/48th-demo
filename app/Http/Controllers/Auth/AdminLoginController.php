<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class AdminLoginController extends Controller
{
	use AuthenticatesUsers;

	protected $redirectTo = '/admin/types';

	public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

	//登入視圖
    public function showLoginForm()
    {
        return view('admin.login');
    }

    //預設帳號欄位
    public function username()
    {
        return 'account';
    }

    protected function guard(){
        return Auth::guard('admin');
    }

}
