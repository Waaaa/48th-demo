<?php

namespace App\Http\Controllers;

use App\Train;
use App\Type;
use App\Route;
use App\Station;
use App\DrivingWeek;
use Session;
use DB;
use DateTime;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class TrainController extends Controller
{
    public function __construct()
	{
	    $this->middleware('auth:admin', ['except' => ['userIndex', 'show', 'searchByNumber', 'NumberSearchResult', 'searchTrain', 'TrainSearchResult']]);
	}

	public function index(){
		$trains = Train::orderBy('departure_time', 'asc')->paginate(10);

		//計算頁數
		if($trains->lastPage() < $trains->currentPage()){
			abort(404,'頁面不存在');
			exit;
		}

		return view('admin.trains.index')->with(['trains' => $trains]);
	}

	public function userIndex(){
		$trains = Train::orderBy('departure_time', 'asc')->paginate(10);

		//計算頁數
		if($trains->lastPage() < $trains->currentPage()){
			abort(404,'頁面不存在');
			exit;
		}

		return view('user.trains.index')->with(['trains' => $trains]);
	}

	public function create(){
		$stations = Station::orderBy('sequence', 'asc')->get();
		return view('admin.trains.create')->with(['stations' => $stations]);
	}

	public function setStation(Request $request){
    	$rules = ['startStation'=>'required|exists:stations,english_name|not_in:'.$request->endStation,
	          	  'endStation'=>'required|exists:stations,english_name|not_in:'.$request->startStation];

	    $this->validate($request, $rules);
	    return redirect()->route('admin.trains.route', ['startStationEngName' => $request->startStation, 'endStationEngName' => $request->endStation]);
	}

	public function route($startStationEngName, $endStationEngName, Request $request){
    	$startStation = Station::where('english_name', $startStationEngName)->firstOrFail();
    	$endStation = Station::where('english_name', $endStationEngName)->firstOrFail();

	    $types = Type::get();
		if($startStation->sequence < $endStation->sequence){
			/* 南下 */
			$stations = Station::where('sequence','>=',$startStation->sequence)->where('sequence','<=',$endStation->sequence)->orderBy('sequence','asc')->get();
		}else{
			/* 北上 */
			$stations = Station::where('sequence','<=',$startStation->sequence)->where('sequence','>=',$endStation->sequence)->orderBy('sequence','desc')->get();
		}
		$arr_weeks = ['一', '二', '三', '四', '五', '六', '日'];

		//行駛星期的欄位預設值設定
		$arr_drivingWeeks = [];
		$i = 1;
		foreach ($arr_weeks as $week) {
			$arr_drivingWeeks[$i-1] = '';
			if(old('drivingWeeks') && in_array($i, old('drivingWeeks'))){
				$arr_drivingWeeks[$i-1] = 'checked';
			}
			$i++;
		}
		return view('admin.trains.route')->with(['startStation' => $startStation, 'endStation' => $endStation, 'types' => $types, 'arr_weeks' => $arr_weeks, 'stations' => $stations, 'arr_drivingWeeks' => $arr_drivingWeeks]);
	    
	}

	public function store(Request $request){
		$count = count($request->station);
		$station = $request->station;
		if($station[0] < $station[$count-1]){
			//南下順序
			$betweenStartNumber = $station[0];
			$betweenEndNumber = $station[$count-1];
		}else{
			//北上順序
			$betweenStartNumber = $station[$count-1];
			$betweenEndNumber = $station[0];
		}
		$rules = ['number'=>'required|numeric|unique:trains,number',
    			  'type'=>'required|exists:types,id',
	              'passengerNumber'=>'required|numeric|min:1',
	          	  'cabinNumber'=>'required|numeric|min:1',
	          	  'drivingWeeks.*'=>'required|numeric|distinct|between:1,7',
	          	  'departureTime'=>'required|date_format:H:i',
	          	  'station.*'=>'required|numeric|distinct|exists:stations,id|between:'.$betweenStartNumber.','.$betweenEndNumber,
				  'stayTime.*'=>'required|numeric|min:0',
	          	  'drivingTime.*'=>'required|numeric|min:0',
	          	  'price.*'=>'required|numeric|min:0'];

	    $this->validate($request, $rules);

		try{
	    	DB::beginTransaction();
	    	//新增列車
	    	$train = new Train();
	    	$train->number = $request->number;
	    	$train->departure_time = $request->departureTime;
	    	$train->passenger_number = $request->passengerNumber;
	    	$train->cabin_number = $request->cabinNumber;
	    	$train->passenger_total_number = $request->passengerNumber*$request->cabinNumber;
	    	$train->start_station_id = $station[0];
	    	$train->end_station_id = $station[$count-1];
	    	$train->type_id = $request->type;
	    	$train->save();

	    	//列車ID
	    	$lastInsertedId = $train->id;

	    	//新增行駛時間
	    	foreach($request->drivingWeeks as $tmp_drivingWeek){
	    		$drivingWeek = new DrivingWeek();
	    		$drivingWeek->week = $tmp_drivingWeek;
	    		$drivingWeek->train_id = $lastInsertedId;
	    		$drivingWeek->save();
	    	}

			//發車站到終點站
			$stayTime = $request->stayTime;
			$drivingTime = $request->drivingTime;
			$price = $request->price;
			for($i = 0 ; $i < $count ; $i++){
				//printf("station id: %s, stayTime: %s, drivingTime: %s, price: %s<br>", $station[$i], $stayTime[$i], $drivingTime[$i], $price[$i]);
				$route = new Route();
				$route->station_id = $station[$i];
				$route->train_id = $lastInsertedId;
				//發車站
				if($i == 0){
					$route->stay_time = 0;
					$route->driving_time = 0;
					$route->price = 0;
					$route->last = 0;
				}else{
					$route->stay_time = $stayTime[$i];
					$route->driving_time = $drivingTime[$i];
					$route->price = $price[$i];
					//終點站
					if($i == ($count-1)){
						$route->last = 1;
					}else{
						$route->last = 0;
					}
				}
				$route->save();
				
			}
			DB::commit();
			Session::flash('success', '新增列車成功');
	    } catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '新增列車失敗，請聯絡網站管理員');
		}
		return redirect()->route('admin.trains.index');
	}

	public function editInfo($train_number){
		//$stations = Station::orderBy('sequence', 'asc')->get();
		$train = Train::where('number', $train_number)->firstOrFail();
		$types = Type::all();
		$arr_weeks = ['一', '二', '三', '四', '五', '六', '日'];

		//行駛星期的欄位預設值設定
		$arr_drivingWeeks = [];
		for($i = 1; $i <= count($arr_weeks); $i++){
			$arr_drivingWeeks[$i-1] = '';
			if(old('drivingWeeks')){
				if(in_array($i, old('drivingWeeks'))){
					$arr_drivingWeeks[$i-1] = 'checked';
				}
			}else{
				if(in_array($i, $train->all_driving_week_number)){
					$arr_drivingWeeks[$i-1] = 'checked';
				}
			}
		}
		
		return view('admin.trains.editInfo')->with(['types' => $types, 'train' => $train, 'arr_weeks' => $arr_weeks, 'arr_drivingWeeks' => $arr_drivingWeeks]);
	}

	public function updateInfo(Request $request, $number){
		$train = Train::where('number', $number)->firstOrFail();
		$rules = ['number'=>'required|numeric|unique:trains,number,'.$number.',number',
    			  'type'=>'required|exists:types,id',
	              'passengerNumber'=>'required|numeric|min:1',
	          	  'cabinNumber'=>'required|numeric|min:1',
	          	  'drivingWeeks.*'=>'required|numeric|distinct|between:1,7',
	          	  'departureTime'=>'required|date_format:H:i'];

	    $this->validate($request, $rules);

		try{
	    	DB::beginTransaction();

	    	//修改列車資訊
	    	$train->number = $request->number;
	    	$train->departure_time = $request->departureTime;
	    	$train->passenger_number = $request->passengerNumber;
	    	$train->cabin_number = $request->cabinNumber;
	    	$train->passenger_total_number = $request->passengerNumber*$request->cabinNumber;
	    	$train->type_id = $request->type;
	    	$train->save();

	    	//修改行駛時間
	    	foreach ($train->drivingWeeks as $tmp_delDrivingWeek) {
	    		$tmp_delDrivingWeek->delete();
	    	}
	    	foreach($request->drivingWeeks as $tmp_drivingWeek){
	    		$drivingWeek = new DrivingWeek();
	    		$drivingWeek->week = $tmp_drivingWeek;
	    		$drivingWeek->train_id = $train->id;
	    		$drivingWeek->save();
	    	}

			DB::commit();
			Session::flash('success', '修改列車 '.$number.' 基本資訊成功');
	    } catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '修改列車 '.$number.' 基本資訊失敗，請聯絡網站管理員');
		}
		return redirect()->route('admin.trains.index');
	}

	public function editStation(Request $request, $number){
		$train = Train::where('number', $number)->firstOrFail();
		$stations = Station::orderBy('sequence', 'asc')->get();

		//發車站與終點站的欄位預設值設定
		$arr_startStationValue = [];
		$arr_endStationValue = [];
		$i = 0;
		foreach ($stations as $station) {
			$arr_startStationValue[$i] = '';
			$arr_endStationValue[$i] = '';
			if(old('startStation') && old('startStation') == $station->english_name){
				$arr_startStationValue[$i] = 'selected';
			}elseif($train->start_station_id == $station->id){
				$arr_startStationValue[$i] = 'selected';
			}
			if(old('endStation') && old('endStation') == $station->english_name){
				$arr_endStationValue[$i] = 'selected';
			}elseif($train->end_station_id == $station->id){
				$arr_endStationValue[$i] = 'selected';
			}
			$i++;
		}

		return view('admin.trains.editStation')->with(['train' => $train, 'stations' => $stations, 'arr_startStationValue' => $arr_startStationValue, 'arr_endStationValue' => $arr_endStationValue]);
	}

	public function updateStation(Request $request, $number){
		$train = Train::where('number', $number)->firstOrFail();
    	$rules = ['startStation'=>'required|exists:stations,english_name|not_in:'.$request->endStation,
	          	  'endStation'=>'required|exists:stations,english_name|not_in:'.$request->startStation];

	    $this->validate($request, $rules);
	    return redirect()->route('admin.trains.edit.route', ['number' => $number, 'startStationEngName' => $request->startStation, 'endStationEngName' => $request->endStation]);
	}

	public function editRoute(Request $request, $number, $startStationEngName, $endStationEngName){
		$train = Train::where('number', $number)->firstOrFail();
		$startStation = Station::where('english_name', $startStationEngName)->firstOrFail();
    	$endStation = Station::where('english_name', $endStationEngName)->firstOrFail();

		if($startStation->sequence < $endStation->sequence){
			/* 南下 */
			$stations = Station::where('sequence','>=',$startStation->sequence)->where('sequence','<=',$endStation->sequence)->orderBy('sequence','asc')->get();
		}else{
			/* 北上 */
			$stations = Station::where('sequence','<=',$startStation->sequence)->where('sequence','>=',$endStation->sequence)->orderBy('sequence','desc')->get();
		}
		
		//停留時間、行駛時間、票價欄位預設值設定
		$arr_stayTimeValue = [];
		$arr_drivingTimeValue = [];
		$arr_priceValue = [];
		$i = 0;
		foreach ($stations as $station) {
			$arr_stayTimeValue[$i] = 0;
			if(old('stayTime.'.$i)){
				$arr_stayTimeValue[$i] = old('stayTime.'.$i);
			}elseif($train->routes->where('station_id', $station->id)->count() > 0){
				$route = $train->routes->where('station_id', $station->id)->first();
				$arr_stayTimeValue[$i] = $route->stay_time;
			}

			$arr_drivingTimeValue[$i] = 0;
			if(old('drivingTime.'.$i)){
				$arr_drivingTimeValue[$i] = old('drivingTime.'.$i);
			}elseif($train->routes->where('station_id', $station->id)->count() > 0){
				$route = $train->routes->where('station_id', $station->id)->first();
				$arr_drivingTimeValue[$i] = $route->driving_time;
			}

			$arr_priceValue[$i] = 0;
			if(old('price.'.$i)){
				$arr_priceValue[$i] = old('price.'.$i);
			}elseif($train->routes->where('station_id', $station->id)->count() > 0){
				$route = $train->routes->where('station_id', $station->id)->first();
				$arr_priceValue[$i] = $route->price;
			}
			$i++;
		}

		return view('admin.trains.editRoute')->with(['train' => $train, 'startStation' => $startStation, 'endStation' => $endStation, 'stations' => $stations, 'arr_stayTimeValue' => $arr_stayTimeValue, 'arr_drivingTimeValue' => $arr_drivingTimeValue, 'arr_priceValue' => $arr_priceValue]);
	}

	public function updateRoute(Request $request, $number){
		$train = Train::where('number', $number)->firstOrFail();
		$count = count($request->station);
		$station = $request->station;
		if($station[0] < $station[$count-1]){
			//南下順序
			$betweenStartNumber = $station[0];
			$betweenEndNumber = $station[$count-1];
		}else{
			//北上順序
			$betweenStartNumber = $station[$count-1];
			$betweenEndNumber = $station[0];
		}
		$rules = ['station.*'=>'required|numeric|distinct|exists:stations,id|between:'.$betweenStartNumber.','.$betweenEndNumber,
				  'stayTime.*'=>'required|numeric|min:0',
	          	  'drivingTime.*'=>'required|numeric|min:0',
	          	  'price.*'=>'required|numeric|min:0'];

	    $this->validate($request, $rules);

		try{
	    	DB::beginTransaction();
	    	//修改列車發車及終點站
	    	$train->start_station_id = $station[0];
	    	$train->end_station_id = $station[$count-1];
	    	$train->save();

	    	//列車ID
	    	$lastInsertedId = $train->id;

	    	//刪除行駛路線
	    	$routes = $train->routes;
			foreach ($routes as $route) {
				$route->delete();
			}

			//重新新增發車站到終點站的行駛路線
			$stayTime = $request->stayTime;
			$drivingTime = $request->drivingTime;
			$price = $request->price;
			for($i = 0 ; $i < $count ; $i++){
				$route = new Route();
				$route->station_id = $station[$i];
				$route->train_id = $lastInsertedId;
				//發車站
				if($i == 0){
					$route->stay_time = 0;
					$route->driving_time = 0;
					$route->price = 0;
					$route->last = 0;
				}else{
					$route->stay_time = $stayTime[$i];
					$route->driving_time = $drivingTime[$i];
					$route->price = $price[$i];
					//終點站
					if($i == ($count-1)){
						$route->last = 1;
					}else{
						$route->last = 0;
					}
				}
				$route->save();
				
			}
			DB::commit();
			Session::flash('success', '修改列車 '.$number.' 行經車站管理成功');
	    } catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '修改列車 '.$number.' 行經車站管理失敗，請聯絡網站管理員');
		}
		return redirect()->route('admin.trains.index');
	}

	public function delete($number){
		$train = Train::where('number', $number)->firstOrFail();
		try{
	    	DB::beginTransaction();

	    	//刪除行駛路線
	    	$routes = $train->routes;
			foreach ($routes as $route) {
				$route->delete();
			}

	    	//刪除行駛時間
	    	foreach ($train->drivingWeeks as $tmp_delDrivingWeek) {
	    		$tmp_delDrivingWeek->delete();
	    	}

	    	//刪除訂票紀錄
	    	foreach ($train->tickets as $tmp_ticket) {
	    		$tmp_ticket->delete();
	    	}

	    	//刪除列車
	    	$train->delete();

			DB::commit();
			Session::flash('success', '刪除列車成功');
	    } catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '刪除列車失敗，請聯絡網站管理員');
		}
		return redirect()->route('admin.trains.index');
	}

    public function searchByNumber()
    {
        return view('user.trains.searchByNumber');
    }

    public function NumberSearchResult(Request $request)
    {
        $rules = ['number'=>'required|numeric|exists:trains,number'];
	    $this->validate($request, $rules);

	    return redirect()->route('user.trains.show', ['train_number' => $request->number]);
    }

    public function searchTrain()
    {
        $stations = Station::orderBy('sequence', 'asc')->get();
        $types = Type::get();
        return view('user.trains.search', ['stations' => $stations, 'types' => $types]);
    }

    public function TrainSearchResult($boardingDate, $startStationEngName, $endStationEngName, $type_id)
    {
    	$input = ['boardingDate'=>$boardingDate,
    			  'startStation'=>$startStationEngName,
	          	  'endStation'=>$endStationEngName,
	          	  'type'=>$type_id];
    	$rules = ['boardingDate'=>'required|date_format:Y-m-d',
    			  'startStation'=>'required|exists:stations,english_name|not_in:'.$endStationEngName,
	          	  'endStation'=>'required|exists:stations,english_name|not_in:'.$startStationEngName,
	          	  'type'=>'required|exists:types,id'];
	    $validator = Validator::make($input, $rules);
	    if($validator->fails()){
	    	return redirect()->route('user.trains.search')
	    			  ->withErrors($validator)
	    			  ->withInput();
	    }

	    //判斷輸入的是否是已過的日期
	    if(strtotime(date('Y-m-d', time())) > strtotime($input['boardingDate'])){
	    	return redirect()->route('user.trains.search')
	    			  ->withErrors(['boardingDate' => '請輸入正確的日期'])
	    			  ->withInput();
	    }

	    //將日期轉換為星期
	    $arr_weekEngNames = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
	    $arr_weeks = ['一', '二', '三', '四', '五', '六', '日'];
	    $week = array_search(date("D",strtotime($boardingDate)), $arr_weekEngNames)+1;

	    //找出起程站、終點站及車種
	    $selectStartStation = Station::where('english_name', $input['startStation'])->firstOrFail();
    	$selectEndStation = Station::where('english_name', $input['endStation'])->firstOrFail();
    	$type = Type::findOrFail($input['type']);

    	//找出所有符合行駛星期的列車ID
	    $obj_matchDrivingWeeksTrainsId = DB::table('driving_weeks')->where('week', $week)->select('train_id')->get()->toArray();
	    $arr_matchDrivingWeeksTrainsId = array_map(function ($value) {
		    return $value->train_id;
		}, $obj_matchDrivingWeeksTrainsId);

	    //找出所有啟程站為發車站的列車
	    $obj_startFromSelectStationTrainsId = DB::table('trains')->where('start_station_id', $selectStartStation->id)->select('id')->get()->toArray();
	    $arr_startFromSelectStationTrainsId = array_map(function ($value) {
		    return $value->id;
		}, $obj_startFromSelectStationTrainsId);

	    //找出所有有停靠起程站或啟程站為發車站的列車ID
		$obj_matchStartStationTrainsId = DB::table('routes')
										->where('station_id', $selectStartStation->id)
										->where(function($query) use ($arr_startFromSelectStationTrainsId){
											//若為發車站，停靠時間為0，所以要特別判斷是否為發車站同起程站的列車
											$query->whereIn('train_id', $arr_startFromSelectStationTrainsId)->orWhere('stay_time', '>', 0);
										})->select('train_id')->get()->toArray();
	    $arr_matchStartStationTrainsId = array_map(function ($value) {
		    return $value->train_id;
		}, $obj_matchStartStationTrainsId);

		 //找出所有到達站為終點站的列車
	    $obj_endFromSelectStationTrainsId = DB::table('trains')->where('end_station_id', $selectEndStation->id)->select('id')->get()->toArray();
	    $arr_endFromSelectStationTrainsId = array_map(function ($value) {
		    return $value->id;
		}, $obj_endFromSelectStationTrainsId);

	    //找出所有有停靠終點站或到達站為終點站的列車ID
		$obj_matchStartAndEndStationTrainsId = DB::table('routes')
												->whereIn('train_id', $arr_matchStartStationTrainsId)
												->where('station_id', $selectEndStation->id)
												->where(function($query) use ($arr_endFromSelectStationTrainsId){
													//若為終點站，停靠時間或為0，所以要特別判斷是否為終點站同到達站的列車
													$query->whereIn('train_id', $arr_endFromSelectStationTrainsId)->orWhere('stay_time', '>', 0);
												})->select('train_id')->get()->toArray();
	    $arr_matchStartAndEndStationTrainsId = array_map(function ($value) {
		    return $value->train_id;
		}, $obj_matchStartAndEndStationTrainsId);

	    //print_r($arr_matchDrivingWeeksTrainsId);
		//print_r($arr_matchStartStationTrainsId);
		//print_r($arr_matchStartAndEndStationTrainsId);

	    //將前面的列車ID結果交集，查詢出列車
		$arr_resultTrainId = array_intersect($arr_matchDrivingWeeksTrainsId, $arr_matchStartAndEndStationTrainsId);
	    $trains = Train::whereIn('id', $arr_resultTrainId)->where('type_id', $input['type'])->orderBy('departure_time', 'asc')->paginate(10);

	    //計算頁數
	    if($trains->lastPage() > 0){
	    	if($trains->lastPage() < $trains->currentPage()){
				abort(404,'頁面不存在');
				exit;
			}
	    }

	    return view('user.trains.result', ['trains' => $trains, 'selectStartStation' => $selectStartStation, 'selectEndStation' => $selectEndStation, 'type' => $type, 'week' => $week, 'arr_weeks' => $arr_weeks, 'boardingDate' => date('Y/m/d', strtotime($input['boardingDate']))]);
    }

    public function show($number){
    	$train = Train::where('number', $number)->firstOrFail();

    	return view('user.trains.show', ['train' => $train]);
    }
}

