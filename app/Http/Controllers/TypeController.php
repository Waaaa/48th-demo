<?php

namespace App\Http\Controllers;


use App\Type;
use DB;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class TypeController extends Controller
{
    public function __construct()
	{
	    $this->middleware('auth:admin');
	}
	
	public function index(){
		$types = Type::get();
		return view('admin.types.index')->with(['types' => $types]);
	}

	public function create(){
		 return view('admin.types.create');
	}

	public function store(Request $request){
    	$rules = ['typeName'=>'required|unique:types,type_name',
	              'highestSpeed'=>'required|numeric|min:1'];
	    $this->validate($request, $rules);

	    try{
	    	DB::beginTransaction();

	    	$type = new Type();
			$type->type_name = $request->typeName;
			$type->highest_speed = $request->highestSpeed;
			$type->save();
			DB::commit();
			Session::flash('success', '車種新增成功');
	    	return redirect()->route('admin.types.index');
	    } catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '新增車種失敗，請聯絡網站管理員');
			return redirect()->route('admin.types.index');
		}
	}

	public function edit($id){
		$type = Type::findOrfail($id);
		return view('admin.types.edit')->with(['type' => $type]);
	}

	public function update(Request $request, $id){
	    $rules = ['typeName'=>'required|unique:types,type_name,'.$id,
	              'highestSpeed'=>'required|numeric|min:1'];
	    $this->validate($request, $rules);

	    try{
	    	DB::beginTransaction();

	    	$type = Type::findOrfail($id);
			$type->type_name = $request->typeName;
			$type->highest_speed = $request->highestSpeed;
			$type->save();
			DB::commit();
			Session::flash('success', '車種修改成功');
			return redirect()->route('admin.types.index');
	    } catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '修改車種失敗，請聯絡網站管理員');
			return redirect()->route('admin.types.index');
		}
	}

	public function delete($id){
		$type = Type::findOrfail($id);
		try{
	    	DB::beginTransaction();

			$trains = $type->trains;
			foreach ($trains as $train) {
				$routes = $train->routes;
				foreach ($routes as $route) {
					$route->delete();
				}
				$train->delete();
			}
			DB::commit();
			$type->delete();
			Session::flash('success', '車種刪除成功');
	    	return redirect()->route('admin.types.index');
	    } catch (Exception $e) {
			DB::rollback();
			Session::flash('error', '成功車種失敗，請聯絡網站管理員');
			return redirect()->route('admin.types.index');
		}
	}
}
