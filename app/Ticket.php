<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //啟程車站
    function start_station(){
    	return $this->hasOne('App\Station', 'id', 'start_station_id');
    }

    //終點車站
    function end_station(){
    	return $this->hasOne('App\Station', 'id', 'end_station_id');
    }

    //訂購的列車
    function train(){
    	return $this->hasOne('App\Train', 'number', 'train_number');
    }

    //accessor
    //定義時間欄位的顯示格式
    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d H:i', strtotime($value));
    }

    public function getCanceledAtAttribute($value)
    {
        return date('Y-m-d H:i', strtotime($value));
    }

    public function getDeletedAtAttribute($value)
    {
        return date('Y-m-d H:i', strtotime($value));
    }
}
