<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{

	//定義關聯
	//該車種的列車
    function trains(){
    	return $this->hasMany('App\Train', 'type_id', 'id');
    }
}
