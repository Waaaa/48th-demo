<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Train extends Model
{

    //定義關聯
    //起始站
    function startStation(){
    	return $this->hasOne('App\Station', 'id', 'start_station_id');
    }
    //終點站
    function endStation(){
    	return $this->hasOne('App\Station', 'id', 'end_station_id');
    }
    //列車停靠資訊
    function routes(){
    	return $this->hasMany('App\Route', 'train_id', 'id')->orderBy('id', 'asc');
    }
    //列車車種
    function type(){
        return $this->hasOne('App\Type', 'id', 'type_id');
    }
    //行駛星期
    function drivingWeeks(){
        return $this->hasMany('App\DrivingWeek', 'train_id', 'id')->orderBy('week', 'asc');
    }
    //訂票紀錄
    function tickets(){
        return $this->hasMany('App\Ticket', 'train_number', 'number')->orderBy('created_at', 'asc');
    }

    //accessor
    public function getAllDrivingWeekChineseNameAttribute(){
        $drivingWeeks = $this->drivingWeeks;
        $arr_weeks = array();
        foreach ($drivingWeeks as $drivingWeek) {
            $arr_weeks[] = $drivingWeek->chinese_name;
        }
        return $arr_weeks;
    }

    public function getAllDrivingWeekNumberAttribute(){
        $drivingWeeks = $this->drivingWeeks;
        $arr_weeks = array();
        foreach ($drivingWeeks as $drivingWeek) {
            $arr_weeks[] = $drivingWeek->week;
        }
        return $arr_weeks;
    }

    //計算各站的預計到達時間、行駛時間及票價
    public function getRouteInfomationAttribute(){
            $departureTime = $this->departure_time;
            $routes = Route::where('train_id', $this->id)->get();
            $startRoute = Route::where('train_id', $this->id)->where('station_id', $this->start_station_id)->first();
            $endRoute = Route::where('train_id', $this->id)->where('station_id', $this->end_station_id)->first();

            $drivingMin = 0;
            $arr_routeInfo = [];
            foreach ($routes as $route) {
                //如果不是發車站就加上前站到此站的行駛時間
                if($route->id != $this->start_station_id){
                    $drivingMin += $route->driving_time;
                }

                //此站的預計到達時間
                $arrivalTime = date('H:i', strtotime($departureTime."+".$drivingMin."min"));

                //此站的行駛時間
                $dteStart = new \DateTime($departureTime);
                $dteEnd = new \DateTime($arrivalTime);
                $dteDiff = date('H:i', (strtotime($arrivalTime) - strtotime($departureTime)));

                //如果不是發車站或終點站就加上此站的停靠時間
                if($route->id != $this->start_station_id || $route->id != $this->end_station_id){
                    $drivingMin += $route->stay_time;
                    //此站的預計出發時間
                    $tmp_departureTime = date('H:i', strtotime($arrivalTime."+".$route->stay_time."min"));
                }else{
                    $tmp_departureTime = $arrivalTime;
                }

                $arr_routeInfo[$route->station_id] = [
                    'arrivalTime' => $arrivalTime,
                    'departureTime' => $tmp_departureTime,
                    'drivingTime' => $dteDiff,
                    'price' => $route->price
                ];
            }

            return $arr_routeInfo;
    }

    //取的兩站間的行駛時間
    public function getDrivingTimeBetweenTwoStation($startStation_id, $endStation_id){
        $departureTime = new \DateTime($this->route_infomation[$startStation_id]['departureTime']);
        $arrivalTime = new \DateTime($this->route_infomation[$endStation_id]['arrivalTime']);
        $timediff = $arrivalTime->diff($departureTime);
        $tmp_h = "";
        $tmp_i = "";
        if($timediff->h < 10){
           $tmp_h = "0";
        }
        if($timediff->i < 10){
           $tmp_i = "0";
        }
        return $timediff->format($tmp_h."%h:".$tmp_i."%i");
    }
}
