@extends('layouts.admin')
@section('main-page')
<div class="row">
	<div class="col-12">
		<nav>
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item">管理後臺</li>
		    <li class="breadcrumb-item active">訂票紀錄查詢</li>
		  </ol>
		</nav>
	</div>
</div>
@if(Session::has('success'))
<div class="row">
	<div class="alert alert-success col-12" role="alert">
	  {{Session::get('success')}}
	</div>
</div>
@elseif(Session::has('error'))
<div class="row">
	<div class="alert alert-danger col-12" role="alert">
	  {{Session::get('error')}}
	</div>
</div>
@endif
<div class="row section-box">
	<form id="form" class="col-12 offset-lg-3 col-lg-6" method="POST" action="{{ route('admin.tickets.search.result') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label><b>發車日期</b><a class="text-danger">*</a></label>
			<input type="date" name="boardingDate" class="form-control" value="{{ old('boardingDate') }}">
			@if($errors->has('boardingDate'))
				<label class="text-danger">{{ $errors->first('boardingDate') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>車次</b><a class="text-danger">*</a></label>
			<input type="number" name="trainNumber" class="form-control" value="{{ old('trainNumber')  }}">
			@if($errors->has('trainNumber'))
				<label class="text-danger">{{ $errors->first('trainNumber') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>手機號碼</b></label>
			<input type="text" name="cellPhoneNumber" class="form-control" value="{{ old('cellPhoneNumber')  }}">
			<label class="text-secondary">手機號碼格式範例：09XX-XXXXXX</label>
			@if($errors->has('cellPhoneNumber'))
				<br>
				<label class="text-danger">{{ $errors->first('cellPhoneNumber') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>訂票編號</b></label>
			<input type="text" name="ticketNumber" class="form-control" value="{{ old('ticketNumber')  }}">
			@if($errors->has('ticketNumber'))
				<label class="text-danger">{{ $errors->first('ticketNumber') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>啟程站</b><a class="text-danger">*</a></label>
			<select id="startStation" name="startStation" class="form-control">
				<option value="" selected>請選擇啟程站</option>
				@foreach($stations as $station)
					@if(old('startStation') == $station->english_name)
						<option data-value="{{ $station->sequence }}" value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
					@else
						<option data-value="{{ $station->sequence }}" value="{{$station->english_name}}">{{ $station->chinese_name }}</option>
					@endif
				@endforeach
			</select>
			@if($errors->has('startStation'))
				<label class="text-danger">{{ $errors->first('startStation') }}</label>
			@endif
	    </div>
	    <div class="form-group">
	    	<label><b>到達站</b><a class="text-danger">*</a></label>
			<select id="endStation" name="endStation" class="form-control">
				<option value="" selected>請選擇到達站</option>
	    		@foreach($stations as $station)
					@if(old('endStation') == $station->english_name)
						<option value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
					@else
						<option value="{{$station->english_name}}">{{ $station->chinese_name }}</option>
					@endif
				@endforeach
			</select>
			@if($errors->has('endStation'))
				<label class="text-danger">{{ $errors->first('endStation') }}</label>
			@endif
	    </div>
		<div class="form-group text-right">
			<button type="submit" class="btn btn-primary">查詢</button>
		</div>
	</form>
</div>
@endsection
@section('js-section')
<script type="text/javascript">

/* 發車站與終點站不重複 */
$("#startStation option[value='"+$("#endStation :selected").val()+"']").attr('disabled', true);
$("#endStation option[value='"+$("#startStation :selected").val()+"']").attr('disabled', true);
$('#startStation').on('change', function() {
	$("#endStation option").attr('disabled', false);
	if(this.value != ""){
		$("#endStation option[value='"+this.value+"']").attr('disabled', true);
	}
});
$('#endStation').on('change', function() {
	$("#startStation option").attr('disabled', false);
	if(this.value != ""){
		$("#startStation option[value='"+this.value+"']").attr('disabled', true);
	}
});
</script>
@endsection