@extends('layouts.admin')
@section('main-page')
<div class="row">
	<nav class="col-12">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="#">管理後臺</a></li>
	    <li class="breadcrumb-item"><a href="{{route('admin.trains.index')}}">列車管理</a></li>
	    <li class="breadcrumb-item">{{ $train->number }}</li>
	    <li class="breadcrumb-item active" aria-current="page">修改列車停靠、停駛車站</li>
	  </ol>
	</nav>	
</div>

<div class="row mb-4">
	<div class="col-12 mb-1">
		<div class="row">
			<div class="col-6 text-center">
				<p class="text-secondary">Step 1. 發車、終點站設定</p>
			</div>
			<div class="col-6 text-center">
				Step 2. 列車行經車站管理
			</div>
		</div>
	</div>
	<div class="col-12">
		<div class="progress">
		  <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 100%"></div>
		</div>
	</div>
	
</div>
<div class="row section-box">
	<form class="col-12 offset-lg-3 col-lg-6" action="{{ route('admin.trains.update.route', ['train_number' => $train->number]) }}" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		{{ method_field('PATCH') }}
		<div class="row text-center mb-5">
			<div class="col-4">
				<h3>{{ $startStation->chinese_name }}</h3>
			</div>
			<div class="col-4">
				<img width="48" height="48" src="{{ asset('images/right-arrow.png') }}">
			</div>
			<div class="col-4">
				<h3>{{ $endStation->chinese_name }}</h3>
			</div>
		</div>
		<hr>
	    <div class="form-group">
	    	@foreach($stations as $station)
	    	<div class="form-row">
			    <div class="col-md-12">
			      <label>#{{ $loop->iteration }} <b>{{ $station->chinese_name }} {{ $loop->iteration == 1 ? '(發車站)' : '' }}{{ $loop->iteration == count($stations) ? '(終點站)' : '' }}</b></label>
			      @if($errors->has('stayTime.'.$loop->index) || $errors->has('drivingTime.'.$loop->index) || $errors->has('price.'.$loop->index))
			      	<label class="text-danger">該車站停留時間、行駛時間或是票價設定錯誤</label>
			      @endif
			      <input type="hidden" name="station[]" value="{{ $station->id }}">
			    </div>
			    <div class="form-group col-12 col-lg-4">
			      <label>停留時間(min)<a class="text-danger">*</a></label>
			      <input type="number" name="stayTime[]" class="form-control plus-time" min="0" value="{{ $arr_stayTimeValue[$loop->index] }}" {{ $loop->iteration == 1 || $loop->iteration == count($stations) ? 'readonly="true"' : '' }}>
			    </div>
			    <div class="form-group col-12 col-lg-5">
			      <label>前站到此的行駛時間(min)<a class="text-danger">*</a></label>
			      <input type="number" name="drivingTime[]" class="form-control plus-time" min="0" value="{{ $arr_drivingTimeValue[$loop->index] }}" {{ $loop->iteration == 1 ? 'readonly="true"' : '' }}>
			    </div>
			    <div class="form-group col-12 col-lg-3">
			      <label>票價<a class="text-danger">*</a></label>
			      <input type="number" name="price[]" class="form-control" min="0" value="{{ $arr_priceValue[$loop->index] }}" {{ $loop->iteration == 1 ? 'readonly="true"' : '' }}>
			    </div>
			</div>
			@endforeach
	    </div>
	    <div class="row">
	    <div class="form-group col-6">
			<a id="back-info" class="btn btn-secondary" href="{{ route('admin.trains.edit.station',['train_number'=>$train->number]) }}">上一步</a>
		</div>
		<div class="form-group col-6 text-right">
			<input type="submit" class="btn btn-primary" value="送出">
		</div>
		</div>
	</form>
</div>
@endsection
@section('js-section')
<script type="text/javascript">


/* 離開頁面前的檢查 */
$(window).bind('beforeunload', function (e) {
    return '資料尚未存檔，確定是否要離開?';
    
})
$("input:submit").click(function () {
    $(window).unbind('beforeunload');
});
$("#back-info").click(function () {
    $(window).unbind('beforeunload');
});

/* 計算最終抵達時間 */
/*
$('.plus-time').on('change', function() {
	var count = 0;
	$(".plus-time").each(function() {
	    //console.log($(this).val());
	    if(parseInt($(this).val()) > 0){
	    	count += parseInt($(this).val());
	    }
	    //count = parseInt($(this).val());
	});
	//console.log(count);
	var date = add_minutes(new Date('1997-12-15 '+$("#startTime").val()), count);
	var year = date.getFullYear().toString();
	var mouth = (date.getMonth() + 1);

	//月份補0
	if(mouth < 10){
		mouth = "0"+mouth.toString();
	}else{
		mouth = mouth.toString();
	}
	var day = date.getDate().toString();
	var hours = date.getHours();

	//小時補0
	if(hours < 10){
		hours = "0"+hours.toString();
	}else{
		hours = hours.toString();
	}

	//分鐘補0
	var minutes = date.getMinutes().toString();
	if(minutes.length < 2){
		minutes = "0"+minutes;
	}
	//console.log(date+"/"+year+"/"+mouth+"/"+day+" "+hours+":"+minutes);
	var time = hours+":"+minutes;
	$("#endTime").val(time);
});

function add_minutes(dt, minutes) {
	return new Date(dt.getTime() + minutes*60000);
}
*/
$("#passengerNumber").on('change', function() {
	count();
});

$("#cabinNumber").on('change', function() {
	count();
});

function count(){
	total = parseInt($("#passengerNumber").val()) * parseInt($("#cabinNumber").val());
	$($("#passengerTotalNumber").val(total));
}

</script>
@endsection