@extends('layouts.admin')
@section('main-page')
<div class="row">
	<nav class="col-12">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="#">管理後臺</a></li>
	    <li class="breadcrumb-item"><a href="{{route('admin.trains.index')}}">列車管理</a></li>
	    <li class="breadcrumb-item active" aria-current="page">新增列車</li>
	  </ol>
	</nav>	
</div>

<div class="row mb-4">
	<div class="col-12 mb-1">
		<div class="row">
			<div class="col-6 text-center">
				Step 1. 發車、終點站設定
			</div>
			<div class="col-6 text-center">
				<p class="text-secondary">Step 2. 列車基本資訊與行經車站管理</p>
			</div>
		</div>
	</div>
	<div class="col-12">
		<div class="progress">
		  <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 50%"></div>
		</div>
	</div>
	
</div>
<div class="row section-box">
	<form class="col-12 offset-lg-3 col-lg-6" action="{{route('admin.trains.setStation')}}" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label><b>發車站</b><a class="text-danger">*</a></label>
			<select id="startStationfield" name="startStation" class="form-control">
			@foreach($stations as $station)
				@if(old('startStation') == $station->english_name)
					<option value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
				@else
					<option value="{{$station->english_name}}">{{ $station->chinese_name }}</option>
				@endif
			@endforeach
			</select>
			@if($errors->has('startStation'))
				<label class="text-danger">{{ $errors->first('startStation') }}</label>
			@endif
	    </div>
	    <div class="form-group">
	    	<label><b>終點站</b><a class="text-danger">*</a></label>
			<select id="endStationfield" name="endStation" class="form-control">
    		@foreach($stations as $station)
				@if(old('endStation') == $station->english_name)
					<option value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
				@elseif(old('endStation') === null && $loop->iteration == 2)
					<option value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
				@else
					<option value="{{$station->english_name}}">{{ $station->chinese_name }}</option>
				@endif
			@endforeach
			</select>
			@if($errors->has('endStation'))
				<label class="text-danger">{{ $errors->first('endStation') }}</label>
			@endif
	    </div>
		<div class="form-group text-right">
			<input type="submit" class="btn btn-info" value="下一步">
		</div>
	</form>
</div>
@endsection
@section('js-section')
<script type="text/javascript">
/* 離開頁面前的檢查 */
$(window).bind('beforeunload', function (e) {
    return '資料尚未存檔，確定是否要離開?';
    
})
$("input:submit").click(function () {
    $(window).unbind('beforeunload');
});

/* 發車站與終點站不重複 */
$("#startStationfield option[value='"+$("#endStationfield :selected").attr('value')+"']").attr('disabled', true);
$("#endStationfield option[value='"+$("#startStationfield :selected").attr('value')+"']").attr('disabled', true);
$('#startStationfield').on('change', function() {
	$("#endStationfield option").attr('disabled', false);
	$("#endStationfield option[value='"+this.value+"']").attr('disabled', true);
});
$('#endStationfield').on('change', function() {
	$("#startStationfield option").attr('disabled', false);
	$("#startStationfield option[value='"+this.value+"']").attr('disabled', true);
});

</script>
@endsection