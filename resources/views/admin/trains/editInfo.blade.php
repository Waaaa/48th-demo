@extends('layouts.admin')
@section('main-page')
<div class="row">
	<nav class="col-12">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="#">管理後臺</a></li>
	    <li class="breadcrumb-item"><a href="{{route('admin.trains.index')}}">列車管理</a></li>
	    <li class="breadcrumb-item">{{ $train->number }}</li>
	    <li class="breadcrumb-item active" aria-current="page">修改列車基本資訊</li>
	  </ol>
	</nav>	
</div>

<div class="row section-box">
	<form class="col-12 offset-lg-3 col-lg-6" action="{{route('admin.trains.update.info', ['number' => $train->number])}}" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		{{ method_field('PATCH') }}
		<div class="form-group">
			<label><b>列車代碼</b></label>
			<input type="number" name="number" class="form-control" value="{{ (old('number')) ? old('number') : $train->number }}">
			@if($errors->has('number'))
				<label class="text-danger">{{ $errors->first('number') }}</label>
			@endif
		</div>
	    <div class="form-group">
	    	<label><b>請選擇車種</b><a class="text-danger">*</a></label>
	    	<select name="type" class="form-control">
	    	@foreach($types as $type)
	    		<option value="{{$type->id}}" {{ (old('type')) ? (old('type') == $type->id) ? 'selected' : '' : ($train->type->id == $type->id) ? 'selected' : '' }}>{{$type->type_name}}</option>
	    	@endforeach
	    	</select>
		</div>
		<div class="form-group">
			<label><b>單一車廂的載客數量</b><a class="text-danger">*</a></label>
			<input type="number" id="passengerNumber" name="passengerNumber" class="form-control" value="{{ (old('passengerNumber')) ? old('passengerNumber') : $train->passenger_number }}">
			@if($errors->has('passengerNumber'))
				<label class="text-danger">{{ $errors->first('passengerNumber') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>車廂數量</b><a class="text-danger">*</a></label>
			<input type="number" id="cabinNumber" name="cabinNumber" class="form-control" value="{{ (old('cabinNumber')) ? old('cabinNumber') : $train->cabin_number }}">
			@if($errors->has('cabinNumber'))
				<label class="text-danger">{{ $errors->first('cabinNumber') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>總載客數</b></label>
			<input type="number" id="passengerTotalNumber" name="passengerTotalNumber" class="form-control" value="{{ (old('passengerTotalNumber')) ? old('passengerTotalNumber') : $train->passenger_total_number }}" readonly="true">
		</div>
		<div class="form-group">
			<label><b>行車星期</b><a class="text-danger">*</a></label>
			@foreach($arr_weeks as $week)
			<div class="form-check">
				<input name="drivingWeeks[]" class="form-check-input" type="checkbox" value="{{$loop->iteration}}" id="checkbox-{{$loop->iteration}}" {{ $arr_drivingWeeks[$loop->index] }}>
				<label class="form-check-label" for="checkbox-{{$loop->iteration}}">
					{{ $week }}
				</label>
			</div>
			@endforeach
			@if($errors->has('drivingWeeks'))
				<label class="text-danger">{{ $errors->first('drivingWeeks') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>發車時間</b><a class="text-danger">*</a></label>
			<input type="time" name="departureTime" class="form-control" value="{{ (old('departureTime')) ? date('H:i', strtotime(old('departureTime'))) : date('H:i', strtotime($train->departure_time)) }}">
			@if($errors->has('departureTime'))
				<label class="text-danger">{{ $errors->first('departureTime') }}</label>
			@endif
		</div>
		<div class="form-group text-right">
			<a href="{{ route('admin.trains.index') }}" onclick="return confirm('確定要取消修改這次的動作嗎? 系統將無法復原您的資料')" class="btn btn-secondary">取消</a>
			<input type="submit" class="btn btn-primary" value="送出">
		</div>
	</form>
</div>
@endsection
@section('js-section')
<script type="text/javascript">
/* 離開頁面前的檢查 */
$(window).bind('beforeunload', function (e) {
    return '資料尚未存檔，確定是否要離開?';
    
})
$("input:submit").click(function () {
    $(window).unbind('beforeunload');
});


function count(){
	total = parseInt($("#passengerNumber").val()) * parseInt($("#cabinNumber").val());
	$($("#passengerTotalNumber").val(total));
}

$("#passengerNumber").on('change', function() {
	count();
});
$("#cabinNumber").on('change', function() {
	count();
});

order();
</script>
@endsection