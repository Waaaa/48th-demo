@extends('layouts.admin')
@section('main-page')
<div class="row">

	<nav class="col-10">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="#">管理後臺</a></li>
	    <li class="breadcrumb-item active" aria-current="page">車種列表</li>
	  </ol>
	</nav>	
	<div class="col-2 text-right pt-2">
		<a href="{{route('admin.types.create')}}" class="btn btn-primary">新增車種</a>	
	</div>
	@if (Session::has('success'))
	<div class="alert alert-success col-12" role="alert">
	  {{Session::get('success')}}
	</div>
	@endif
	@if (Session::has('error'))
	<div class="alert alert-danger col-12" role="alert">
	  {{Session::get('error')}}
	</div>
	@endif
</div>


<div class="row section-box">
	<table class="table text-center">
		<thead>
			<tr id="first-line">
				<td></td>
				<td>車種名稱</td>
				<td>最高時速(km/h)</td>
				<td></td>
			</tr>
		</thead>
		<tbody>
		@forelse($types as $type)
			<tr>
				<td><b>#{{ $loop->iteration }}</b></td>
				<td>{{ $type->type_name }}</td>
				<td>{{ $type->highest_speed }}</td>
				<td>
					<form method="POST" action="{{route('admin.types.delete',[ 'id' => $type->id ])}}">
						{{ method_field('DELETE') }}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<a class="btn btn-warning" href="{{route('admin.types.edit',[ 'id' => $type->id ])}}">修改</a>
						<button type="submit" class="btn btn-danger" onclick="return confirm('確定要刪除這筆資料嗎? 系統將無法復原您的刪除')">刪除</button>
					</form>
				</td>	
			</tr>
		@empty
			<tr>
				<td colspan="4" align="center" class="text-secondary">目前沒有任何車種</td>
			</tr>
		@endforelse
		</tbody>
	</table>
</div>
@endsection