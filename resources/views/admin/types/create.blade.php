@extends('layouts.admin')
@section('main-page')
<div class="row">
	<nav class="col-12">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="#">管理後臺</a></li>
	    <li class="breadcrumb-item"><a href="{{route('admin.types.index')}}">車種管理</a></li>
	    <li class="breadcrumb-item active" aria-current="page">新增車種</li>
	  </ol>
	</nav>	
</div>


<div class="row section-box">
	<form class="col-12 offset-lg-3 col-lg-6" action="{{route('admin.types.store')}}" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label><b>車種名稱</b><a class="text-danger">*</a></label>
			<input type="text" name="typeName" class="form-control" value="{{ old('typeName') }}">
			@if($errors->has('typeName'))
				<label class="text-danger">{{ $errors->first('typeName') }}</label>
			@endif
		</div>
		
		<div class="form-group">
			<label><b>最高時速(km/h)</b><a class="text-danger">*</a></label>
			<input type="number" name="highestSpeed" class="form-control" value="{{ old('highestSpeed') }}">
			@if($errors->has('highestSpeed'))
				<label class="text-danger">{{ $errors->first('highestSpeed') }}</label>
			@endif
		</div>
		<div class="form-group text-right">
			<a href="{{ route('admin.types.index') }}" onclick="return confirm('確定要取消修改這次的動作嗎? 系統將無法復原您的資料')" class="btn btn-secondary">取消</a>
			<button type="submit" class="btn btn-primary">新增</button>
		</div>
	</form>
</div>
@endsection