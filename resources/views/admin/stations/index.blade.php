@extends('layouts.admin')
@section('main-page')
<div class="row">

	<nav class="col-10">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="#">管理後臺</a></li>
	    <li class="breadcrumb-item active" aria-current="page">車站列表</li>
	  </ol>
	</nav>	
	<div class="col-2 text-right pt-2">
		<a href="{{route('admin.stations.create')}}" class="btn btn-primary">新增車站</a>	
	</div>
	@if (Session::has('success'))
	<div class="alert alert-success col-12" role="alert">
	  {{Session::get('success')}}
	</div>
	@endif
	@if (Session::has('error'))
	<div class="alert alert-danger col-12" role="alert">
	  {{Session::get('error')}}
	</div>
	@endif
</div>


<div class="row section-box">
	<table class="table text-center">
		<thead>
			<tr id="first-line">
				<td>車站排序(北下)</td>
				<td>中文站名</td>
				<td>英文站名</td>
				<td></td>
			</tr>
		</thead>
		<tbody>
		@forelse($stations as $station)
			<tr>
				<td><b>#{{ $loop->iteration }}</b></td>
				<td>{{ $station->chinese_name }}</td>
				<td>{{ $station->english_name }}</td>
				<td>
					<form method="POST" action="{{route('admin.stations.delete',[ 'id' => $station->id ])}}">
						{{ method_field('DELETE') }}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<a class="btn btn-warning" href="{{route('admin.stations.edit',[ 'id' => $station->id ])}}">修改</a>
						<button station="submit" class="btn btn-danger" onclick="return confirm('確定要刪除這筆資料嗎? 系統將無法復原您的刪除')">刪除</button>
					</form>
				</td>	
			</tr>
		@empty
			<tr>
				<td colspan="4" align="center" class="text-secondary">目前沒有任何車站</td>
			</tr>
		@endforelse
		</tbody>
	</table>
</div>
@endsection