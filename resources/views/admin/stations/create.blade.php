@extends('layouts.admin')
@section('main-page')
<div class="row">
	<nav class="col-12">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="#">管理後臺</a></li>
	    <li class="breadcrumb-item"><a href="{{route('admin.stations.index')}}">車站管理</a></li>
	    <li class="breadcrumb-item active" aria-current="page">新增車站</li>
	  </ol>
	</nav>	
</div>


<div class="row section-box">
	<form class="col-12 offset-lg-3 col-lg-6" action="{{route('admin.stations.store')}}" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label><b>中文站名</b><a class="text-danger">*</a></label>
			<input type="text" name="chineseName" class="form-control" value="{{ old('chineseName') }}">
			@if($errors->has('chineseName'))
				<label class="text-danger">{{ $errors->first('chineseName') }}</label>
			@endif
		</div>
	
		<div class="form-group">
			<label><b>英文站名</b><a class="text-danger">*</a></label>
			<input type="text" name="englishName" class="form-control" value="{{ old('englishName') }}">
			@if($errors->has('englishName'))
				<label class="text-danger">{{ $errors->first('englishName') }}</label>
			@endif
		</div>
	    
		<div class="form-group">
    		<label><b>新增於 (北下順序)</b><a class="text-danger">*</a></label>
    		<select name="sequence" class="form-control">
    			<option value="0">新增為第一個車站</option>
    			@foreach ($stations as $station)
    				<option value="{{$station->sequence}}">{{ $station->chinese_name }}   之後</option>
    			@endforeach
    		</select>
    		@if($errors->has('sequence'))
				<label class="text-danger">{{ $errors->first('sequence') }}</label>
			@endif
    	</div>	
		<div class="form-group text-right">
			<a href="{{ route('admin.stations.index') }}" onclick="return confirm('確定要取消修改這次的動作嗎? 系統將無法復原您的資料')" class="btn btn-secondary">取消</a>
			<button type="submit" class="btn btn-primary">新增</button>
		</div>
	</form>
</div>
@endsection