@extends('layouts.user')
@section('main-page')
<nav>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">火車訂票系統</a></li>
    <li class="breadcrumb-item active" aria-current="page">管理者登入</li>
  </ol>
</nav>
<div class="row section-box ">

	<form method="POST" action="{{ route('admin.login') }}" class="col-12 offset-lg-3 col-lg-6">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		@if ($errors->has('account') || $errors->has('password'))
	    	<div class="alert alert-danger" role="alert">
				帳號或密碼錯誤
			</div>
	    @endif
		<div class="form-group">
			<label><b>帳號</b></label>
			<input type="text" name="account" class="form-control">
		</div>
		<div class="form-group">
			<label><b>密碼</b></label>
			<input type="password" name="password" class="form-control">
		</div>	
		<div class="form-group text-right">
			<button type="submit" class="btn btn-dark">登入</button>
		</div>
	</form>
</div>
<br>
<center><p style="cursor: pointer;" data-toggle="modal" data-target="#defaultUserModal">系統預設帳號一覽</p></center>
<div class="modal fade" id="defaultUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle"><b>系統預設帳號一覽</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        系統預設管理者帳號: admin
        <br>
        系統預設管理者密碼: cdertgbvf
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">返回</button>
      </div>
    </div>
  </div>
</div>
@endsection