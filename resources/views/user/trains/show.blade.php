@extends('layouts.user')
@section('main-page')
<nav>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">火車訂票系統</a></li>
    <li class="breadcrumb-item">列車資訊</li>
    <li class="breadcrumb-item active" aria-current="page">{{ $train->number }}</li>
  </ol>
</nav>

<div class="row section-box">
	<div class="col-12">
		<table class="table text-center">
			<thead>
				<tr class="first-line">
					<th>車種</th>
					<th>車次</th>
					<th>發車站</th>
					<th>終點站</th>
					<th>預計開車時間</th>
					<th>預計到達時間</th>
					<th>行駛時間</th>
					<th>行車星期</th>
			</thead>
			<tbody>
				<tr>
					<td>{{ $train->type->type_name }}</td>
					<td>{{ $train->number }}</td>
					<td>{{ $train->startStation->chinese_name }}</td>
					<td>{{ $train->endStation->chinese_name }}</td>
					<td>{{ date('H:i', strtotime($train->departure_time)) }}</td>
					<td>{{ $train->route_infomation[$train->end_station_id]['arrivalTime'] }}</td>
					<td>{{ $train->route_infomation[$train->end_station_id]['drivingTime'] }}</td>
					<td>{{ implode('、', $train->all_driving_week_chinese_name) }}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-12">
		<div class="row">
		@foreach($train->routes as $route)
    		<div class="col-md-4 col-lg-2 text-center">
    			<div class="row mb-2">
        			<div class="col-8">
	        			<b>{{$route->station->chinese_name}}</b>
	        			<br>
	        			@if($route->station_id == $train->start_station_id || $route->stay_time > 0 || $route->station_id == $train->end_station_id)
	        				@if($route->station_id == $train->start_station_id)
	        					-<br>
	        					{{ $train->route_infomation[$route->station_id]['arrivalTime'] }} 發車<br>
	        					-
	        				@elseif($route->station_id == $train->end_station_id)
	        					{{ $train->route_infomation[$route->station_id]['arrivalTime'] }} 抵達<br>
	        					-<br>
	        					TWD {{number_format($train->route_infomation[$route->station_id]['price'])}}
	        				@else
	        					{{ $train->route_infomation[$route->station_id]['arrivalTime'] }} 抵達<br>
	        					{{ $train->route_infomation[$route->station_id]['departureTime'] }} 發車<br>
	        					TWD {{number_format($train->route_infomation[$route->station_id]['price'])}}
	        				@endif
	        			@else
	        				<a class="text-danger">不停靠</a>
	        			@endif
        			</div>
        			@if($loop->iteration < count($train->routes))
        			<div class="col">
        				<i class="fas fa-arrow-right"></i>
        			</div>
        			@endif
    			</div>
    		</div>
    	@endforeach
		</div>
	</div>
	<div class="col-12 text-right">
		<form action="{{ route('user.tickets.create') }}" method="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="trainNumber" value="{{ $train->number }}">
			<input type="hidden" name="startStation" value="{{ $train->startStation->english_name }}">
			<input type="hidden" name="endStation" value="{{ $train->endStation->english_name }}">
			<button type="submit" class="btn btn-primary">訂票</button>
		</form>
	</div>
</div>
@endsection

