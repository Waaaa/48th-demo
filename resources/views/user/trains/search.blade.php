@extends('layouts.user')
@section('main-page')
<nav>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">火車訂票系統</a></li>
    <li class="breadcrumb-item active" aria-current="page">車次查詢</li>
  </ol>
</nav>

<div class="row section-box">
	<form id="form" class="col-12 offset-lg-3 col-lg-6" method="GET" onsubmit="return checkForm(this);">
		<div class="form-group">
			<label><b>啟程站</b><a class="text-danger">*</a></label>
			<select id="startStationfield" name="startStation" class="form-control">
			@foreach($stations as $station)
				@if(old('startStation') == $station->english_name)
					<option data-value="{{ $station->sequence }}" value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
				@else
					<option data-value="{{ $station->sequence }}" value="{{$station->english_name}}">{{ $station->chinese_name }}</option>
				@endif
			@endforeach
			</select>
			@if($errors->has('startStation'))
				<label class="text-danger">{{ $errors->first('startStation') }}</label>
			@endif
	    </div>
		<div class="form-group">
	    	<label><b>到達站</b><a class="text-danger">*</a></label>
			<select id="endStationfield" name="endStation" class="form-control">
    		@foreach($stations as $station)
				@if(old('endStation') == $station->english_name)
					<option value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
				@elseif(old('endStation') === null && $loop->iteration == 2)
					<option value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
				@else
					<option value="{{$station->english_name}}">{{ $station->chinese_name }}</option>
				@endif
			@endforeach
			</select>
			@if($errors->has('endStation'))
				<label class="text-danger">{{ $errors->first('endStation') }}</label>
			@endif
	    </div>
	    <div class="form-group">
	    	<label><b>請選擇車種</b><a class="text-danger">*</a></label>
	    	<select name="type" class="form-control">
	    	@foreach($types as $type)
	    		<option value="{{$type->id}}" {{ (old('type') == $type->id) ? 'selected' : '' }}>{{$type->type_name}}</option>
	    	@endforeach
	    	</select>
	    	@if($errors->has('type'))
				<label class="text-danger">{{ $errors->first('type') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>搭乘日期</b><a class="text-danger">*</a></label>
			<input type="date" name="date" class="form-control" min="{{ date('Y-m-d') }}" value="{{ old('date') ? old('date') : date('Y-m-d') }}">
			@if($errors->has('boardingDate'))
				<label class="text-danger">{{ $errors->first('boardingDate') }}</label>
			@endif
		</div>
		<div class="form-group text-right">
			<button type="submit" class="btn btn-primary">查詢</button>
		</div>
	</form>
</div>
@endsection
@section('js-section')
<script type="text/javascript">

/* 發車站與終點站不重複 */
$("#startStationfield option[value='"+$("#endStationfield :selected").val()+"']").attr('disabled', true);
$("#endStationfield option[value='"+$("#startStationfield :selected").val()+"']").attr('disabled', true);
$('#startStationfield').on('change', function() {
	$("#endStationfield option").attr('disabled', false);
	$("#endStationfield option[value='"+this.value+"']").attr('disabled', true);
});
$('#endStationfield').on('change', function() {
	$("#startStationfield option").attr('disabled', false);
	$("#startStationfield option[value='"+this.value+"']").attr('disabled', true);
});


var checkForm = function(form){
	//console.log(form);
	var startStationVal = $("#startStationfield :selected").val();
	var endStationVal = $("#endStationfield :selected").val();
	var typeVal = $("select[name=type] :selected").val();
	var dateVal = $("input[name=date]").val();

	var arr_currentPath = window.location.href.split("/");
	arr_currentPath.pop();
	var formAction = arr_currentPath.join("/")+"/trains/result/"+dateVal+"/"+startStationVal+"/"+endStationVal+"/"+typeVal;
	$("#form").attr("action", formAction);
	console.log(formAction);
	window.location.replace(formAction);
	return false;
}
</script>
@endsection
