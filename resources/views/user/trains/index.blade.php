@extends('layouts.user')
@section('main-page')
<div class="row">
	<nav class="col-8 col-md-6">
	  <ol class="breadcrumb">
		<li class="breadcrumb-item">火車訂票系統</li>
		<li class="breadcrumb-item active" aria-current="page">列車資訊</li>
	  </ol>
	</nav>
	<div class="col-4 col-md-6 text-right pt-2">
		<a href="{{ route('user.trains.search.by.number') }}" class="btn btn-info">查詢列車資訊</a>
	</div>
	@if (Session::has('success'))
	<div class="alert alert-success col-12" role="alert">
	  {{Session::get('success')}}
	</div>
	@endif
	@if (Session::has('error'))
	<div class="alert alert-danger col-12" role="alert">
	  {{Session::get('error')}}
	</div>
	@endif
</div>


<div class="row section-box">
	<div class="col-12">
		<table class="table text-center">
			<thead>
				<tr class="first-line">
					<th></th>
					<th>車種</th>
					<th>車次</th>
					<th>發車站</th>
					<th>終點站</th>
					<th>預計開車時間</th>
					<th>預計到達時間</th>
					<th>行駛時間</th>
					<th>行車星期</th>
				</tr>
			</thead>
			<tbody>
				@forelse($trains as $train)
				<tr>
					<td>{{ ($trains->currentPage()-1)*10+$loop->iteration }}</td>
					<td>{{ $train->type->type_name }}</td>
					<td><a href="{{ route('user.trains.show', ['train_number' => $train->number]) }}" target="_blank">{{ $train->number }}</a></td>
					<td>{{ $train->startStation->chinese_name }}</td>
					<td>{{ $train->endStation->chinese_name }}</td>
					<td>{{ date('H:i', strtotime($train->departure_time)) }}</td>
					<td>{{ $train->route_infomation[$train->end_station_id]['arrivalTime'] }}</td>
					<td>{{ $train->route_infomation[$train->end_station_id]['drivingTime'] }}</td>
					<td>{{ implode('、', $train->all_driving_week_chinese_name) }}</td>
				</tr>
				@empty
				<tr>
					<td colspan="9" class="text-secondary">查無指定條件車次</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
	@if($trains->lastPage() > 1)
		<div class="col-12">
	        <nav>
	          <ul class="pagination justify-content-end">
	            @if( $trains->currentPage() != 1 )
	              <li class="page-item"><a class="page-link" href="{{ $trains->url($trains->currentPage()-1) }}"><</a></li>
	            @endif
	            @for($i = 1 ; $i <= $trains->lastPage(); $i++)
	              <li class="page-item {{ ($trains->currentPage() == $i) ? 'active' : '' }}"><a class="page-link" href="{{ $trains->url($i) }}">{{ $i }}</a></li>
	            @endfor
	            @if( $trains->currentPage() != $trains->lastPage() )
	              <li class="page-item"><a class="page-link" href="{{ $trains->nextPageUrl() }}">></a></li>
	            @endif
	          </ul>
	        </nav>
	    </div>
    @endif
</div>
@endsection