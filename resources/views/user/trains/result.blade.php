@extends('layouts.user')
@section('main-page')
<div class="row">

	<nav class="col-12">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">火車訂票系統</li>
	    <li class="breadcrumb-item">車次查詢</li>
	    <li class="breadcrumb-item active" aria-current="page">查詢結果</li>
	  </ol>
	</nav>	
</div>


<div class="row section-box">
	<div class="col-12 mb-4">
		<b>查詢條件：&emsp;
		{{ $selectStartStation->chinese_name }}&ensp;到&ensp;{{ $selectEndStation->chinese_name }}
		&ensp;
		{{ $boardingDate }}(星期{{ $arr_weeks[$week-1] }})
		</b>
	</div>
	<div class="col-12">
		<table class="table text-center">
			<thead>
				<tr class="first-line">
					<th></th>
					<th>車種</th>
					<th>車次</th>
					<th>發車站</th>
					<th>終點站</th>
					<th>預計開車時間</th>
					<th>預計到達時間</th>
					<th>行駛時間</th>
					<th>票價</th>
					<th></th>
			</thead>
			<tbody>
				@forelse($trains as $train)
				<tr>
					<th>{{ ($trains->currentPage()-1)*10+$loop->iteration }}</th>
					<td>{{ $train->type->type_name }}</td>
					<td><a href="{{ route('user.trains.show', ['train_number' => $train->number]) }}" target="_blank">{{ $train->number }}</a></td>
					<td>{{ $train->startStation->chinese_name }}</td>
					<td>{{ $train->endStation->chinese_name }}</td>
					<td>{{ $train->route_infomation[$selectStartStation->id]['departureTime'] }}</td>
					<td>{{ $train->route_infomation[$selectEndStation->id]['arrivalTime'] }}</td>
					<td>{{ $train->getDrivingTimeBetweenTwoStation($selectStartStation->id, $selectEndStation->id) }}</td>
					<td>TWD {{ number_format($train->route_infomation[$selectEndStation->id]['price'] - $train->route_infomation[$selectStartStation->id]['price']) }}</td>
					<td>
						<form action="{{ route('user.tickets.create') }}" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="trainNumber" value="{{ $train->number }}">
							<input type="hidden" name="startStation" value="{{ $selectStartStation->english_name }}">
							<input type="hidden" name="endStation" value="{{ $selectEndStation->english_name }}">
							<button type="submit" class="btn btn-primary">訂票</button>
						</form>
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="10" class="text-secondary">查無指定條件車次</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
	<div class="col-12 d-flex justify-content-between">
		<a href="{{ route('user.trains.search') }}" class="btn btn-secondary">重新查詢</a>
		@if($trains->lastPage() > 1)
        <nav>
          <ul class="pagination justify-content-end">
            @if( $trains->currentPage() != 1 )
              <li class="page-item"><a class="page-link" href="{{ $trains->url($trains->currentPage()-1) }}"><</a></li>
            @endif
            @for($i = 1 ; $i <= $trains->lastPage(); $i++)
              <li class="page-item {{ ($trains->currentPage() == $i) ? 'active' : '' }}"><a class="page-link" href="{{ $trains->url($i) }}">{{ $i }}</a></li>
            @endfor
            @if( $trains->currentPage() != $trains->lastPage() )
              <li class="page-item"><a class="page-link" href="{{ $trains->nextPageUrl() }}">></a></li>
            @endif
          </ul>
        </nav>
      @endif
	</div>
</div>
@endsection