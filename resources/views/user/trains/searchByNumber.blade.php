@extends('layouts.user')
@section('main-page')
<nav>
  <ol class="breadcrumb">
    <li class="breadcrumb-item">火車訂票系統</li>
    <li class="breadcrumb-item">列車資訊</li>
    <li class="breadcrumb-item active" aria-current="page">查詢列車資訊</li>
  </ol>
</nav>

<div class="row section-box">
	<form id="form" class="col-12 offset-lg-3 col-lg-6" method="POST" action="{{ route('user.trains.number.search.result') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label><b>車次</b><a class="text-danger">*</a></label>
			<input type="number" name="number" class="form-control" value="{{ old('number')  }}">
			@if($errors->has('number'))
				<label class="text-danger">查無車次</label>
			@endif
		</div>
		<div class="form-group text-right">
			<button type="submit" class="btn btn-primary">查詢</button>
		</div>
	</form>
</div>
@endsection
