@extends('layouts.user')
@section('main-page')
<nav>
  <ol class="breadcrumb">
    <li class="breadcrumb-item">火車訂票系統</li>
    <li class="breadcrumb-item active" aria-current="page">預訂車票</li>
  </ol>
</nav>

<div class="row section-box">
	<form id="form" class="col-12 offset-lg-3 col-lg-6" method="POST" action="{{ route('user.tickets.store') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label><b>手機號碼</b><a class="text-danger">*</a></label>
			<input type="text" name="cellPhoneNumber" class="form-control" value="{{ old('cellPhoneNumber')  }}">
			<label class="text-secondary">手機號碼格式範例：09XX-XXXXXX</label>
			@if($errors->has('cellPhoneNumber'))
				<br>
				<label class="text-danger">{{ $errors->first('cellPhoneNumber') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>啟程站</b><a class="text-danger">*</a></label>
			<select id="startStation" name="startStation" class="form-control">
			@foreach($stations as $station)
				@if(empty($input['startStation']) ? old('startStation') == $station->english_name : $input['startStation'] == $station->english_name)
					<option data-value="{{ $station->sequence }}" value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
				@else
					<option data-value="{{ $station->sequence }}" value="{{$station->english_name}}">{{ $station->chinese_name }}</option>
				@endif
			@endforeach
			</select>
			@if($errors->has('startStation'))
				<label class="text-danger">{{ $errors->first('startStation') }}</label>
			@endif
	    </div>
	    <div class="form-group">
	    	<label><b>到達站</b><a class="text-danger">*</a></label>
			<select id="endStation" name="endStation" class="form-control">
    		@foreach($stations as $station)
				@if(empty($input['endStation']) ? old('endStation') == $station->english_name : $input['endStation'] == $station->english_name)
					<option value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
				@elseif((empty($input['endStation']) ? old('endStation') === null : $input['endStation'] === null) && $loop->iteration == 2)
					<option value="{{$station->english_name}}" selected>{{ $station->chinese_name }}</option>
				@else
					<option value="{{$station->english_name}}">{{ $station->chinese_name }}</option>
				@endif
			@endforeach
			</select>
			@if($errors->has('endStation'))
				<label class="text-danger">{{ $errors->first('endStation') }}</label>
			@endif
	    </div>
	    <div class="form-group">
			<label><b>搭乘日期</b><a class="text-danger">*</a></label>
			<input type="date" name="boardingDate" class="form-control" min="{{ date('Y-m-d') }}" value="{{ old('boardingDate') ? old('boardingDate') : date('Y-m-d') }}">
			@if($errors->has('boardingDate'))
				<label class="text-danger">{{ $errors->first('boardingDate') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>車次</b><a class="text-danger">*</a></label>
			<input type="number" name="trainNumber" class="form-control" value="{{ empty($input['trainNumber']) ? old('trainNumber') : $input['trainNumber'] }}">
			@if($errors->has('trainNumber'))
				<label class="text-danger">{{ $errors->first('trainNumber') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>車票張數</b><a class="text-danger">*</a></label>
			<input type="number" name="numberOfTickets" class="form-control" min="1" max="100" value="{{ old('numberOfTickets') ? old('numberOfTickets') : 1 }}">
			<label class="text-secondary">請輸入1~100內的整數數字</label>
			@if($errors->has('numberOfTickets'))
				<br>
				<label class="text-danger">{{ $errors->first('numberOfTickets') }}</label>
			@endif
		</div>
		<div class="form-group text-right">
			<button type="submit" class="btn btn-primary">訂票</button>
		</div>
	</form>
</div>
@endsection
@section('js-section')
<script type="text/javascript">

/* 發車站與終點站不重複 */
$("#startStation option[value='"+$("#endStation :selected").val()+"']").attr('disabled', true);
$("#endStation option[value='"+$("#startStation :selected").val()+"']").attr('disabled', true);
$('#startStation').on('change', function() {
	$("#endStation option").attr('disabled', false);
	$("#endStation option[value='"+this.value+"']").attr('disabled', true);
});
$('#endStation').on('change', function() {
	$("#startStation option").attr('disabled', false);
	$("#startStation option[value='"+this.value+"']").attr('disabled', true);
});
</script>
@endsection