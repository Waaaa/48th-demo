@extends('layouts.user')
@section('main-page')
<div class="row">

	<nav class="col-12">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">火車訂票系統</li>
	    <li class="breadcrumb-item">訂票查詢</li>
	    <li class="breadcrumb-item active" aria-current="page">查詢結果</li>
	  </ol>
	</nav>	
</div>


<div class="row section-box">
	<div class="col-12 mb-4">
		<b>查詢條件：&emsp;
		@if(!empty($ticketNumber))
			訂位編號：{{ $ticketNumber }}&ensp;
		@endif
		@if(!empty($cellPhoneNumber))
			手機號碼：{{ $cellPhoneNumber }}
		@endif
		</b>
	</div>
	<div class="col-12">
		<table class="table text-center">
			<thead>
				<tr class="first-line">
					<th></th>
					<th>訂票編號</th>
					<th>訂票時間</th>
					<th>搭乘日期</th>
					<th>發車時間</th>
					<th>車次</th>
					<th>啟程站</th>
					<th>到達站</th>
					<th>訂購張數</th>
					<th></th>
			</thead>
			<tbody>
				@forelse($tickets as $ticket)
				<tr>
					<th>{{ ($tickets->currentPage()-1)*10+$loop->iteration }}</th>
					<td><a href="{{ route('user.tickets.show', ['ticketNumber' => $ticket->number]) }}" target="_blank">{{ $ticket->number }}</a></td>
					<td>{{ $ticket->created_at }}</td>
					<td>{{ $ticket->boarding_date }}</td>
					<td>{{ $ticket->train->route_infomation[$ticket->start_station_id]['departureTime'] }}</td>
					<td>{{ $ticket->train->number }}</td>
					<td>{{ $ticket->start_station->chinese_name }}</td>
					<td>{{ $ticket->end_station->chinese_name }}</td>
					<td>{{ $ticket->number_of_tickets }}</td>
					<td>
						@if($ticket->is_cancel)
							<p class="text-danger">已取消訂票<br>{{ $ticket->canceled_at }}</p>
						@elseif($ticket->is_delete)
							<p class="text-danger">已由網站管理員取消訂票<br>{{ $ticket->deleted_at }}</p>
						@elseif(strtotime($ticket->boarding_date." ".$ticket->train->route_infomation[$ticket->start_station_id]['departureTime']." -30min") > time())
							<form method="POST" action="{{ route('user.tickets.cancel', ['ticketNumber' => $ticket->number]) }}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="_method" value="DELETE">
								<button type="submit" class="btn btn-danger" onclick="return confirm('確定要取消訂票嗎? 系統將無法復原您的動作')">取消訂票</button>
							</form>
						@elseif(strtotime($ticket->boarding_date." ".$ticket->train->route_infomation[$ticket->start_station_id]['departureTime']) <= time())
							<p class="text-success">列車已出發</p>
						@endif
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="10" class="text-secondary">查無訂票紀錄</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
	<div class="col-12 d-flex justify-content-between">
		<a href="{{ route('user.tickets.search') }}" class="btn btn-secondary">重新查詢</a>
		@if($tickets->lastPage() > 1)
        <nav>
          <ul class="pagination justify-content-end">
            @if( $tickets->currentPage() != 1 )
              <li class="page-item"><a class="page-link" href="{{ $tickets->url($tickets->currentPage()-1) }}"><</a></li>
            @endif
            @for($i = 1 ; $i <= $tickets->lastPage(); $i++)
              <li class="page-item {{ ($tickets->currentPage() == $i) ? 'active' : '' }}"><a class="page-link" href="{{ $tickets->url($i) }}">{{ $i }}</a></li>
            @endfor
            @if( $tickets->currentPage() != $tickets->lastPage() )
              <li class="page-item"><a class="page-link" href="{{ $tickets->nextPageUrl() }}">></a></li>
            @endif
          </ul>
        </nav>
      @endif
	</div>
</div>
@endsection