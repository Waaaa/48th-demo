@extends('layouts.user')
@section('main-page')
<nav>
  <ol class="breadcrumb">
    <li class="breadcrumb-item">火車訂票系統</li>
    <li class="breadcrumb-item active" aria-current="page">訂票查詢</li>
  </ol>
</nav>
@if($errors->has('ticket'))
<div class="row">
	<div class="alert alert-danger col-12" role="alert">
	  {{ $errors->first('ticket') }}
	</div>
</div>
@endif
<div class="row section-box">
	<form id="form" class="col-12 offset-lg-3 col-lg-6" method="POST" action="{{ route('user.tickets.search.result') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label><b>訂票編號</b></label>
			<input type="text" name="ticketNumber" class="form-control" value="{{ old('ticketNumber')  }}">
			@if($errors->has('ticketNumber'))
				<label class="text-danger">{{ $errors->first('ticketNumber') }}</label>
			@endif
		</div>
		<div class="form-group">
			<label><b>手機號碼</b></label>
			<input type="text" name="cellPhoneNumber" class="form-control" value="{{ old('cellPhoneNumber')  }}">
			<label class="text-secondary">手機號碼格式範例：09XX-XXXXXX</label>
			@if($errors->has('cellPhoneNumber'))
				<br>
				<label class="text-danger">{{ $errors->first('cellPhoneNumber') }}</label>
			@endif
		</div>
		<div class="form-group text-right">
			<button type="submit" class="btn btn-primary">查詢</button>
		</div>
	</form>
</div>
@endsection