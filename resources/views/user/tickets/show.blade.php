@extends('layouts.user')
@section('main-page')
<nav>
  <ol class="breadcrumb">
    <li class="breadcrumb-item">火車訂票系統</li>
    <li class="breadcrumb-item">預訂車票</li>
    <li class="breadcrumb-item active" aria-current="page">訂位明細</li>
  </ol>
</nav>
@if(Session::has('success'))
<div class="row">
	<div class="alert alert-success col-12" role="alert">
	  {{Session::get('success')}}
	</div>
</div>
@elseif(Session::has('error'))
<div class="row">
	<div class="alert alert-danger col-12" role="alert">
	  {{Session::get('error')}}
	</div>
</div>
@endif
<div class="row section-box">
	<div class="col-12">
		<table class="table table-bordered text-center">
			<tr>
				<td class="table-active">訂票編號</td>
				<td>{{ $ticket->number }}</td>
				<td class="table-active">手機號碼</td>
				<td>{{ $ticket->cell_phone_number }}</td>
			</tr>
			<tr>
				<td class="table-active">訂購張數</td>
				<td>{{ $ticket->number_of_tickets }}</td>
				<td class="table-active">車票單價</td>
				<td>TWD {{ number_format($ticket->train->route_infomation[$ticket->end_station_id]['price'] - $ticket->train->route_infomation[$ticket->start_station_id]['price']) }}</td>
			</tr>
			@if($ticket->is_cancel)
				<tr>
					<td colspan="4"><b class="text-danger">已於{{ $ticket->canceled_at }}取消訂票</b></td>
				</tr>
			@elseif($ticket->is_delete)
				<tr>
					<td colspan="4"><b class="text-danger">已於{{ $ticket->deleted_at }}由網站管理員取消訂票</b></td>
				</tr>
			@elseif(strtotime($ticket->boarding_date." ".$ticket->train->route_infomation[$ticket->start_station_id]['departureTime']) <= time())
				<tr>
					<td colspan="4"><b class="text-success">列車已於{{ $ticket->boarding_date." ".$ticket->train->route_infomation[$ticket->start_station_id]['departureTime'] }}出發</b></td>
				</tr>
							
			@endif
		</table>
	</div>
	<div class="col-12 mt-4">
		<table class="table table-bordered text-center">
			<tr class="table-active">
				<th>搭乘日期</th>
				<th>車次</th>
				<th>啟程站</th>
				<th>到達站</th>
				<th>預計開車時間</th>
				<th>預計到達時間</th>
				<th>行駛時間</th>
				<th>車種</th>
			</tr>
			<tr>
				<td>{{ $ticket->boarding_date }}</td>
				<td><a href="{{ route('user.trains.show', ['train_number' => $ticket->train->number]) }}" target="_blank">{{ $ticket->train->number }}</a></td>
				<td>{{ $ticket->start_station->chinese_name }}</td>
				<td>{{ $ticket->end_station->chinese_name }}</td>
				<td>{{ $ticket->train->route_infomation[$ticket->start_station_id]['departureTime'] }}</td>
				<td>{{ $ticket->train->route_infomation[$ticket->end_station_id]['arrivalTime'] }}</td>
				<td>{{ $ticket->train->getDrivingTimeBetweenTwoStation($ticket->start_station_id, $ticket->end_station_id) }}</td>
				<td>{{ $ticket->train->type->type_name }}</td>
			</tr>
		</table>
	</div>
	<div class="col-12 mt-4 text-right">
		<p>
			<b class="text-danger">總票價：TWD {{ number_format(($ticket->train->route_infomation[$ticket->end_station_id]['price'] - $ticket->train->route_infomation[$ticket->start_station_id]['price'])*$ticket->number_of_tickets) }}</b>
		</p>
		<hr>
	</div>
	<div class="col-12 d-flex justify-content-between">
		<a href="{{ route('user.trains.index') }}" class="btn btn-secondary">回到首頁</a>
		@if(!$ticket->is_cancel && !$ticket->is_delete && strtotime($ticket->boarding_date." ".$ticket->train->route_infomation[$ticket->start_station_id]['departureTime']." -30min") > time())
			<form method="POST" action="{{ route('user.tickets.cancel', ['ticketNumber' => $ticket->number]) }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="_method" value="DELETE">
				<button type="submit" class="btn btn-danger" onclick="return confirm('確定要取消訂票嗎? 系統將無法復原您的動作')">取消訂票</button>
			</form>
		@endif
	</div>
</div>
@endsection
