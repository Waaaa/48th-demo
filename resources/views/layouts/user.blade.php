<!DOCTYPE html>
<html>
<head>
	<title>火車訂票系統</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('css/index.css')}}"></link>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
		<a class="navbar-brand" style="line-height: 64px;" href="#">
			<img src="{{asset('images/train.png')}}" width="64" height="64" class="d-inline-block align-top" alt="">
			火車訂票系統
		</a>
		
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
			    	<a class="nav-link" href="{{route('user.trains.search')}}">首頁</a>
			  	</li>
			    <li class="nav-item">
			    	<a class="nav-link" href="{{route('user.tickets.create')}}">預訂車票</a>
			    </li>
			    <li class="nav-item">
					<a class="nav-link" href="{{route('user.tickets.search')}}">訂票查詢</a>
				</li>
				<li class="nav-item">
			    	<a class="nav-link" href="{{route('user.trains.index')}}">列車資訊</a>
			  	</li>		  
			</ul>
			@if(!Auth::guard('admin')->check())
				<ul class="navbar-nav my-2 my-lg-0">
			  		<li class="nav-item">
				    	<a class="nav-link" href="{{ route('admin.login.show') }}">登入後台</a>
				  	</li>
				</ul>
			@else
				<ul class="navbar-nav my-2 my-lg-0">
			  		<li class="nav-item">
				    	<a class="nav-link" href="{{ route('admin.trains.index') }}">進入後台</a>
				  	</li>
				  	<li class="nav-item">
						<a class="nav-link" href="{{route('admin.login.logout')}}">登出</a>
					</li>
				</ul>
			@endif
			
		</div>
	</div>	
</nav>

<section id="main-page">
	<div class="container">
	<!-- main page -->
	 @yield('main-page')
	</div>
</section>


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

@yield('js-section')
</html>